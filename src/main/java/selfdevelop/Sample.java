//package selfdevelop;
//
//import org.deckfour.xes.in.XesXmlParser;
//import org.deckfour.xes.model.XAttributeMap;
//import org.deckfour.xes.model.XLog;
//import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
//import org.deckfour.xes.out.XSerializer;
//import org.deckfour.xes.out.XesXmlSerializer;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//public class Sample {
//    public XLog readXESFile(String path) {
//        File file = new File(path);
//
//        XesXmlParser parser = new XesXmlParser();
//        List<XLog> result = new ArrayList<XLog>();
//        try {
//            result = parser.parse(file);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result.get(0);
//    }
//
//    public void exportSample(XLog sample, String output) {
//        XSerializer export = new XesXmlSerializer();
//        try {
//            export.serialize(sample, new FileOutputStream(output));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void sampling(String input, String output, int sampleSize) {
//        XLog originalLog = this.readXESFile(input);
//        Random rand = new Random();
//        int originSize = originalLog.size();
//        for (int i = 0; i < originSize - sampleSize; i++) {
//            int randomKickOut = rand.nextInt(originalLog.size());
//            originalLog.remove(randomKickOut);
//        }
//        XAttributeMap attrMap = originalLog.getAttributes();
//        attrMap.put("concept:name", new XAttributeLiteralImpl("concept:name", attrMap.get("concept:name").toString().replace(".", "selfdevelope.Sample.")));
//        exportSample(originalLog, output);
//    }
//}
