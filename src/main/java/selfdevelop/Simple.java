package selfdevelop;

import fr.lip6.move.pnml.framework.utils.ModelRepository;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;
import fr.lip6.move.pnml.framework.utils.exception.VoidRepositoryException;
import fr.lip6.move.pnml.pnmlcoremodel.hlapi.*;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Simple {
    public static void main(String[] args) throws IOException, VoidRepositoryException, InvalidIDException {
        String xesFilePath = "./example-logs/repairExample.xes";

        List<String> considerField = new ArrayList<String>();
        considerField.add("concept:name");
        considerField.add("lifecycle:transition");
        
        String outputPath = "./pnml/result.pnml";
        alphaAlgorithm(considerField, xesFilePath, outputPath);        
    }
    
    public static void alphaAlgorithm(List<String> considerField, String xesFilePath, String outputPath) throws InvalidIDException, VoidRepositoryException, IOException  {
        List<XLog> result = readXESFile(xesFilePath);
        int[][] matrix = createFootpath(result, considerField);
        
        Set<String> conceptListOri = getAttributesList(result, considerField);
        ArrayList<String> conceptList = new ArrayList<String>(conceptListOri);
        ModelRepository.getInstance().createDocumentWorkspace("alphaAlgo");
        PetriNetHLAPI newPetriNet = new PetriNetHLAPI("repairExample", PNTypeHLAPI.PTNET);;
        PageHLAPI mainPage = new PageHLAPI("mainPage", newPetriNet);

        List<List<List<Integer>>> setPlace = createSetPlace(matrix);
        createArc(setPlace, conceptList, matrix, mainPage, result, considerField);
        saveFilePNML(outputPath, newPetriNet);
    }
    
    public static List<XLog> readXESFile(String path) {
        File file = new File(path);

        XesXmlParser parser = new XesXmlParser();
        List<XLog> result = new ArrayList<XLog>();
        try {
            result = parser.parse(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
    public static void printFootpath(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++)
                System.out.print(matrix[i][j] + "\t");
            System.out.println();
        }
    }

    public static void saveFilePNML(String outputPath, PetriNetHLAPI newPetriNet) throws IOException {
        String content = newPetriNet.toPNML();
        FileWriter out = new FileWriter(outputPath);
        out.write("<pnml>");
        out.write(content);
        out.write("</pnml>");
        out.close();
    }

    public static Set<String> getAttributesList(List<XLog> result, List<String> fields) {
        Set<String> attributes = new HashSet<String>();

        for (XLog log : result) {
            for (XTrace trace : log) {
                for (XEvent event : trace) {
                    String totalField = "";
                    boolean head = true;
                    for (String field : fields) {
                        if (head) {
                            totalField = totalField.concat(event.getAttributes().get(field).toString());
                            head = false;
                        } else {
                            totalField = totalField.concat("+" + event.getAttributes().get(field).toString());
                        }
                    }
                    attributes.add(totalField);
                }
            }
        }
        return attributes;
    }

    public static List<List<List<Integer>>> temporarySetPlace(List<List<List<Integer>>> setPlace, int[][] matrix, int order) {
        List<List<List<Integer>>> setPlace2 = new ArrayList<List<List<Integer>>>();
        List<List<List<Integer>>> removedList = new ArrayList<List<List<Integer>>>();
        for (int i = 0; i < setPlace.size(); i++) {
            boolean single = true;
            for (int j = i + 1; j < setPlace.size(); j++) {
                if (setPlace.get(i).get(order).equals(setPlace.get(j).get(order))) {
                    single = false;
                    if(matrix[setPlace.get(i).get((order + 1) % 2).get(0)][setPlace.get(j).get((order + 1) % 2).get(0)] == 0) {
                        List<Integer> combi = new ArrayList<Integer>();
                        combi.add(setPlace.get(i).get((order + 1) % 2).get(0));
                        combi.add(setPlace.get(j).get((order + 1) % 2).get(0));
                        List<List<Integer>> pairLR = new ArrayList<List<Integer>>();
                        
                        if (order % 2 == 0) {
                            pairLR.add(setPlace.get(i).get(order)); pairLR.add(combi);
                        }
                        else {
                            pairLR.add(combi); pairLR.add(setPlace.get(i).get(order));
                        }
                        setPlace2.add(pairLR);

                        removedList.add(setPlace.get(i));
                        removedList.add(setPlace.get(j));
                        if (setPlace2.contains(setPlace.get(i)))
                            setPlace2.remove(setPlace.get(i));
                        if (setPlace2.contains(setPlace.get(j)))
                            setPlace2.remove(setPlace.get(j));
                    } else {
                        if(!removedList.contains(setPlace.get(i)) && !setPlace2.contains(setPlace.get(i)))
                            setPlace2.add(setPlace.get(i));
                        if(!removedList.contains(setPlace.get(j)) && !setPlace2.contains(setPlace.get(j)))
                            setPlace2.add(setPlace.get(j));
                    }
                }
            }
            if(single)
                if(!removedList.contains(setPlace.get(i)) && !setPlace2.contains(setPlace.get(i)))
                    setPlace2.add(setPlace.get(i));
        }

        return setPlace2;
    }

    public static List<List<List<Integer>>> createSetPlace(int[][] matrix) {
        List<List<List<Integer>>> setPlace = new ArrayList<List<List<Integer>>>();

        // create pair of each dependency pair transition
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] == 1) {
                    List<Integer> left = new ArrayList<Integer>();
                    left.add(i);
                    List<Integer> right = new ArrayList<Integer>();
                    right.add(j);
                    List<List<Integer>> pairLR = new ArrayList<List<Integer>>();
                    pairLR.add(left); pairLR.add(right);
                    setPlace.add(pairLR);
                }
            }
        }
        
        List<List<List<Integer>>> setPlace2 = temporarySetPlace(setPlace, matrix, 0);

        return temporarySetPlace(setPlace2, matrix, 1);
    }

    public static Map<String, PlaceHLAPI> createPlace(List<List<List<Integer>>> setPlace, PageHLAPI mainPage) {
        Map<String, PlaceHLAPI> placeMap = new HashMap<String, PlaceHLAPI>();
        try {
            placeMap.put("first", new PlaceHLAPI("first", mainPage));
            for (List<List<Integer>> pair : setPlace) {
                placeMap.put(pair.toString(), new PlaceHLAPI(pair.toString(), mainPage));
            }
            placeMap.put("last", new PlaceHLAPI("last", mainPage));
        } catch (InvalidIDException e) {
            e.printStackTrace();
        } catch (VoidRepositoryException e) {
            e.printStackTrace();
        }
        return placeMap;
    }

    public static void createArc(List<List<List<Integer>>> setPlace, List<String> conceptList, int[][] matrix, PageHLAPI mainPage, List<XLog> result, List<String> fields) throws InvalidIDException, VoidRepositoryException {
        Map<String, ArcHLAPI> arcMap = new HashMap<String, ArcHLAPI>();
        Map<String, PlaceHLAPI> placeMap = createPlace(setPlace, mainPage);
        List<Integer> startList = getStartList(result, fields);
        List<Integer> endList = getEndList(result, fields);

        List<TransitionHLAPI> transitionList = new ArrayList<TransitionHLAPI>();

        for (String concept: conceptList)
            transitionList.add(new TransitionHLAPI(concept, mainPage));

        for (int startPoint: startList) {
            arcMap.put("first" + Integer.toString(startPoint), new ArcHLAPI("first" + startPoint, placeMap.get("first"), transitionList.get(startPoint), mainPage));
        }
        for (int endPoint: endList) {
            arcMap.put("last" + Integer.toString(endPoint), new ArcHLAPI("last" + endPoint, transitionList.get(endPoint), placeMap.get("last"), mainPage));
        }

        for (List<List<Integer>> pair: setPlace) {
            for (int input : pair.get(0))
                arcMap.put(pair.toString(), new ArcHLAPI("i" + pair.toString() + "-" + input, transitionList.get(input), placeMap.get(pair.toString()), mainPage));
            for (int output : pair.get(1))
                arcMap.put(pair.toString(), new ArcHLAPI("o" + pair.toString() + "-" + output, placeMap.get(pair.toString()), transitionList.get(output), mainPage));
        }
    }

    public static List<Integer> getStartList(List<XLog> result, List<String> fields) {
        Set<String> conceptListOri = getAttributesList(result, fields);
        ArrayList<String> conceptList = new ArrayList<String>(conceptListOri);
        List<Integer> startList = new ArrayList<Integer>();

        for (XLog log : result) {
            for (XTrace trace : log) {
                String totalField = "";
                boolean head = true;
                for (String field: fields) {
                    if (head) {
                        totalField = totalField.concat(trace.get(0).getAttributes().get(field).toString());
                        head = false;
                    } else {
                        totalField = totalField.concat("+" + trace.get(0).getAttributes().get(field).toString());
                    }
                }
                int item = conceptList.indexOf(totalField);
                if (!startList.contains(item))
                    startList.add(item);
            }
        }
        return startList;
    }

    public static List<Integer> getEndList(List<XLog> result, List<String> fields) {
        Set<String> conceptListOri = getAttributesList(result, fields);
        ArrayList<String> conceptList = new ArrayList<String>(conceptListOri);
        List<Integer> endList = new ArrayList<Integer>();

        for (XLog log : result) {
            for (XTrace trace : log) {
                int traceSize = trace.size();
                String totalField = "";
                boolean head = true;
                for (String field: fields) {
                    if (head) {
                        totalField = totalField.concat(trace.get(traceSize - 1).getAttributes().get(field).toString());
                        head = false;
                    } else {
                        totalField = totalField.concat("+" + trace.get(traceSize - 1).getAttributes().get(field).toString());
                    }
                }
                int item = conceptList.indexOf(totalField);
                if (!endList.contains(item))
                    endList.add(item);
            }
        }
        return endList;
    }

    public static int[][] createFootpath(List<XLog> result, List<String> fields) {
        Set<String> conceptListOri = getAttributesList(result, fields);
        ArrayList<String> conceptList = new ArrayList<String>(conceptListOri);
        System.out.println(conceptList);
        int matrixSize = conceptList.size();
        int[][] matrix = new int[matrixSize][matrixSize];

        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                matrix[i][j] = 0;

        for (XLog log : result) {
            for (XTrace trace : log) {
                int traceSize = trace.size();
                for (int i = 0; i < traceSize - 1; i++) {
                    String totalField1 = "";
                    String totalField2 = "";
                    boolean head = true;
                    for (String field: fields) {
                        if (head) {
                            totalField1 = totalField1.concat(trace.get(i).getAttributes().get(field).toString());
                            totalField2 = totalField2.concat(trace.get(i + 1).getAttributes().get(field).toString());
                            head = false;
                        } else {
                            totalField1 = totalField1.concat("+" + trace.get(i).getAttributes().get(field).toString());
                            totalField2 = totalField2.concat("+" + trace.get(i + 1).getAttributes().get(field).toString());
                        }
                    }
                    int item1 = conceptList.indexOf(totalField1);
                    int item2 = conceptList.indexOf(totalField2);

                    if (matrix[item1][item2] == 0 && matrix[item2][item1] == 0 && item1 == item2)
                        matrix[item1][item2] = 1;
                    else if (matrix[item1][item2] == 0 && matrix[item2][item1] == 0) {
                        matrix[item1][item2] = 1;
                        matrix[item2][item1] = -1;
                    }
                    else if (matrix[item1][item2] == -1 && matrix[item2][item1] == 1) {
                        matrix[item1][item2] = 2;
                        matrix[item2][item1] = 2;
                    }
                }
            }
        }
        return matrix;
    }
}
