package selfdevelop;

import selfdevelop.analysis.VariantAnalysis;
import selfdevelop.casestudy.CaseStudy;
//import selfdevelop.mage.connector.CoreBapeConnector;
//import selfdevelop.mage.Mage;
import selfdevelop.simulate.SimulationArtifact;
import selfdevelop.utils.*;

import java.util.HashMap;
import java.util.Map;

public class Test {
    public final Map<String, Integer> artifactStorage = new HashMap<>();
    private static void TestGenerateProcessInstance() {
//        ModifyTestbenchWiring simulate = new ModifyTestbenchWiring();
//        simulate.genProcessInstance(1000);
        SimulationArtifact simulateArf = new SimulationArtifact();
        simulateArf.genProcessInstance(10);
    }
    private static void testGitlab() {
        ArtifactGitlabToXES controller = new ArtifactGitlabToXES();
        controller.transform();
    }

    private static void testBape() {
        ArtifactBapeToXES controller = new ArtifactBapeToXES();
        controller.transform();
    }

    private static void testJira() {
        ArtifactJiraToXES controller = new ArtifactJiraToXES();
        controller.transform();
    }

    private static void testCombination(String processName) {
        ArtifactCombineEventLogToXES controller = new ArtifactCombineEventLogToXES();
        controller.transformReal(processName, "../processmining/");
    }

    private static void testTaskCoordinate(String processName, String repoLink, CaseStudy caseStudy, String user, String userRealName, String token, String dir) {
        // create new project instance
//        Mage mage = new Mage("create", "Team Leader", processName, user, userRealName, "github.com", token, repoLink, dir, "core_bape");
//        mage.watchProject();

        // connect to available project instance
//        Mage mage2 = new Mage("8e271426-b6ea-497f-8abe-8c7d0a397a78.json", processName, user, userRealName, "github.com", token, repoLink, dir, "core_bape");
//        mage2.watchProject();
//        String commitMessage = "end task Create Views;Views Module:defined:1";
//        mage.detectArtifactFromCommit(commitMessage);
    }

    public static void processModelTest() {
//        TimeAnalysis timeAnalysis = new TimeAnalysis("EcommerceOntoDevObj");
//        timeAnalysis.analyze();

//        RewindAnalysis rewindAnalysis = new RewindAnalysis("EcommerceOntoDevObj");
//        rewindAnalysis.analyze();

        VariantAnalysis variantAnalysis = new VariantAnalysis("EcommerceOntoDevObj");
        variantAnalysis.analyze();
    }

    public static void processModelExportTest() {
        DrawIOExport drawIOExport = new DrawIOExport("EcommerceOntoDevObj");
        drawIOExport.exportProcessVariant();
    }

    public static void main (String[] args) throws Exception {
//        String processName = "Shopping Website";
//        CaseStudy shoppingCaseStudy = new Shopping(processName);
//        shoppingCaseStudy.createConcept();
//        String user = "JosephNguyenNazareth";
//        String userRealName = "TL1";
//        String token = "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";
//        String dir = "/home/ngmkhoi/Documents/workspace/github/MageWebsite";
//        testTaskCoordinate(processName,"https://github.com/JosephNguyenNazareth/MageWebsite", shoppingCaseStudy, user, userRealName, token, dir);

        processModelExportTest();
    }

}
