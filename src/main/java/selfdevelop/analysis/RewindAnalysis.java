package selfdevelop.analysis;

import selfdevelop.type.ProcessModel;
import selfdevelop.utils.DFS;

public class RewindAnalysis {
    DFS dfs;
    public RewindAnalysis(String processName) {
        ProcessModel processModel = new ProcessModel(processName);
        processModel.discover();
        this.dfs = new DFS(processModel);
    }

    public RewindAnalysis(ProcessModel processModel) {
        this.dfs = new DFS(processModel);
    }

    public void analyze() {
        this.dfs.search();
        System.out.println(this.dfs);
    }
}
