package selfdevelop.analysis;

import selfdevelop.type.ActivityBlock;
import selfdevelop.type.AdjacentActivityBlock;
import selfdevelop.type.ProcessModel;

import java.util.HashMap;
import java.util.Map;

public class TimeAnalysis {
    public ProcessModel processModel;
    public TimeAnalysis(String processName) {
        this.processModel = new ProcessModel(processName);
        this.processModel.discover();
    }

    public TimeAnalysis(ProcessModel processModel) {
        this.processModel = processModel;
    }
    public void analyze() {
        Map<String, Double> avgTimeActivity = new HashMap<>();
        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            ActivityBlock actBlock = this.processModel.getActivityBlockMap().get(activityName);
            Map<String, AdjacentActivityBlock> inActivityBlockList =  actBlock.getIn();
            if (!avgTimeActivity.containsKey(activityName))
                avgTimeActivity.put(activityName, allAverage(inActivityBlockList));
        }
        System.out.println(avgTimeActivity);
    }

    private Double allAverage(Map<String, AdjacentActivityBlock> inActivityBlockList) {
        Double sum = 0.0;
        int count = 0;
        for (AdjacentActivityBlock inAct: inActivityBlockList.values()) {
            for (Double execTime: inAct.getExecTimeList()) {
                sum += execTime;
                count++;
            }
        }
        return sum/count;
    }
}
