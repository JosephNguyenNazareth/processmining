package selfdevelop.analysis;

import selfdevelop.type.ActivityBlock;
import selfdevelop.type.AdjacentActivityBlock;
import selfdevelop.type.ProcessModel;

import java.util.Map;
public class VariantAnalysis {
    public ProcessModel processModel;
    public VariantAnalysis(String processName) {
        this.processModel = new ProcessModel(processName);
        this.processModel.discover();
    }

    public VariantAnalysis(ProcessModel processModel) {
        this.processModel = processModel;
    }

    public void analyze() {
        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            ActivityBlock actBlock = this.processModel.getActivityBlockMap().get(activityName);
            Map<String, AdjacentActivityBlock> inActivityBlockList = actBlock.getIn();
            Integer sumInVariant = actBlock.getNbInVariant();
            Integer sumOutVariant = actBlock.getNbOutVariant();
            for (AdjacentActivityBlock inActivity : inActivityBlockList.values()) {
                inActivity.setProb((double)inActivity.getExecTimeList().size()/sumInVariant);
            }

            Map<String, AdjacentActivityBlock> outActivityBlockList = actBlock.getOut();
            for (AdjacentActivityBlock outActivity : outActivityBlockList.values()) {
                outActivity.setProb((double) outActivity.getExecTimeList().size()/sumOutVariant);
            }
        }
//        System.out.println(this.processModel.getActivityBlockMap());
    }
}
