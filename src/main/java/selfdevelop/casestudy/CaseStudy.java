package selfdevelop.casestudy;

import selfdevelop.extend.TermDetect;
import selfdevelop.type.Concept;

public interface CaseStudy {
    public void createConcept();
    public String checkRelevant(String relevantTerm, TermDetect termDetector);
}
