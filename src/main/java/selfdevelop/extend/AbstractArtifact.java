package selfdevelop.extend;

import selfdevelop.casestudy.CaseStudy;
import selfdevelop.utils.Levenshtein;

import java.util.*;

public class AbstractArtifact {
    private static float similarEventBetweenObjects(List<String> eventList1, List<String> eventList2) {
        int differenceEvent = Levenshtein.computeDistanceStringList(eventList1, eventList2);
        return 1 - ((float)differenceEvent)/eventList1.size();
    }
    public static void groupInProject(Map<String, List<String>> objectRegroup, List<String> listObjects, Map<String, List<String>> artifactEvent, CaseStudy realCaseStudy, TermDetect commitDetector) {
        // logic
        // Step 1. Check the similarity between each object event trace. If these object have similar enough event trace, move to step 2
        // Step 2. Check the lavenshtein distance between the path of each object. If these path are closed enough, move to step 3
        // Step 3. Check the importance of the object with the case study. If these object are important enough, do not group these in group but make a new group; otherwise, add to the current group
        for (int i = 0; i < listObjects.size() - 1; i++) {
            for (int j = i + 1; j < listObjects.size(); j++) {
                if (similarEventBetweenObjects(artifactEvent.get(listObjects.get(i)), artifactEvent.get(listObjects.get(j))) >= 0.8) {
//                    int distance = Levenshtein.computeDistance(listObjects.get(i), listObjects.get(j));
//                    float percent = ( (float) distance)/listObjects.get(i).length();
//                    if (percent < 0.2) {
                    String result = realCaseStudy.checkRelevant(listObjects.get(j), commitDetector);
                    if (result.equals("")) {
                        if (!objectRegroup.containsKey(listObjects.get(i)))
                            objectRegroup.put(listObjects.get(i), new ArrayList<>());
                        objectRegroup.get(listObjects.get(i)).add(listObjects.get(j));
                    } else {
                        objectRegroup.put(listObjects.get(j), new ArrayList<>());
                    }
                }
            }
            if (!objectRegroup.containsKey(listObjects.get(i)))
                objectRegroup.put(listObjects.get(i), new ArrayList<>());
        }
        System.out.println("done");
    }

    private static String similarTokens(List<String> tokenList1, List<String> tokenList2) {
        StringBuilder shareTokens = new StringBuilder();
        for (String token1 : tokenList1) {
            for (String token2 : tokenList2) {
                if (token1.equals(token2) && !token1.equals("/")) {
                    shareTokens.append(token1).append(" ");
                }
            }
        }
        return shareTokens.toString();
    }

    public static void groupInterProjects(Map<String, List<String>> objectRegroup, Map<String, List<String>> interProjectGroup) {
        List<String> objectCenters = new ArrayList<>(objectRegroup.keySet());
        List<String> addedObjects = new ArrayList<>();

        TermDetect detector = new TermDetect();
        for (int i = 0; i < objectCenters.size() - 1; i++) {
            for (int j = i + 1; j < objectCenters.size(); j++ ) {
                try {
                    Map<String, String> tokenMap1 = detector.detectLemmaEnglish(objectCenters.get(i));
                    Map<String, String> tokenMap2 = detector.detectLemmaEnglish(objectCenters.get(j));
                    String shareTokens = similarTokens(new ArrayList<>(tokenMap1.keySet()), new ArrayList<>(tokenMap2.keySet()));

                    if (!shareTokens.equals("")) {
                        if (!interProjectGroup.containsKey(shareTokens)) {
                            interProjectGroup.put(shareTokens, new ArrayList<>());
                        }

                        if (!addedObjects.contains(objectCenters.get(i))) {
                            interProjectGroup.get(shareTokens).add(objectCenters.get(i));
                            addedObjects.add(objectCenters.get(i));
                        }
                        if (!addedObjects.contains(objectCenters.get(j))) {
                            interProjectGroup.get(shareTokens).add(objectCenters.get(j));
                            addedObjects.add(objectCenters.get(j));
                        }
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
