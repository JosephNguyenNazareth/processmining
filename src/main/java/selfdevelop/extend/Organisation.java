package selfdevelop.extend;

import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.deckfour.xes.classification.XEventClass;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.*;
import selfdevelop.type.Artifact;
import selfdevelop.utils.XESUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.ml.clustering.Cluster;

public class Organisation {
    private Map<String, XLog> processLog = new HashMap<>();
    private int numberOfEvent = 0;
    private Map<String, List<Artifact>> mappingTaskArtifacts = new HashMap<>();
    private Map<String, List<String>> mappingTaskArtifacts2 = new HashMap<>();

    private Object[] initialiseOrgTable(List<XEventClass> eventClasses, List<XEventClass> resourceClasses) {
        Map<String, Map<String, Double>> orgTable = new HashMap<>();
        Map<String, Integer> eventTotal = new HashMap<>();
        for (XEventClass event : eventClasses) {
            String eventName = event.getId().replace("+", "");
            eventTotal.put(eventName, 0);
        }

        for (XEventClass resource : resourceClasses) {
            Map<String, Double> eventEachResource = new HashMap<>();
            for (XEventClass event : eventClasses) {
                String eventName = event.getId().replace("+", "");
                eventEachResource.put(eventName, 0.0);
            }
            String resourceName = resource.getId().replace("+", "");
            orgTable.put(resourceName, eventEachResource);
        }
        return new Object[]{orgTable, eventTotal};
    }

    private Object[] initialiseActorTotal(List<XEventClass> eventClasses, List<XEventClass> resourceClasses) {
        Map<String, Map<String, Double>> orgTable = new HashMap<>();
        Map<String, Integer> actorTotal = new HashMap<>();
        for (XEventClass actor : resourceClasses) {
            String actorName = actor.getId().replace("+", "");
            actorTotal.put(actorName, 0);
        }
        for (XEventClass resource : resourceClasses) {
            Map<String, Double> eventEachResource = new HashMap<>();
            for (XEventClass event : eventClasses) {
                String eventName = event.getId().replace("+", "");
                eventEachResource.put(eventName, 0.0);
            }
            String resourceName = resource.getId().replace("+", "");
            orgTable.put(resourceName, eventEachResource);
        }
        return new Object[]{orgTable, actorTotal};
    }

    private void computingAppearance(Map<String, Map<String, Double>> orgTable, Map<String, Integer> eventTable) {
        for (Map.Entry<String, XLog> eachLog : processLog.entrySet()) {
            XLog log = eachLog.getValue();
            for (XTrace trace : log) {
                List<String> appear = new ArrayList<>();
                for (XEvent event : trace) {
                    String eventName = event.getAttributes().get("concept:name").toString();
                    String resourceName = event.getAttributes().get("org:resource").toString();
                    Map<String, Double> currentValue = orgTable.get(resourceName);
                    currentValue.put(eventName, currentValue.get(eventName) + 1);
                    orgTable.put(resourceName, currentValue);

                    if (!appear.contains(eventName)) {
                        eventTable.put(eventName, eventTable.get(eventName) + 1);
                        appear.add(eventName);
                    }
                }
            }
        }
    }

    private void computingActorAppearance(Map<String, Map<String, Double>> orgTable, Map<String, Integer> actorTable) {
        for (Map.Entry<String, XLog> eachLog : processLog.entrySet()) {
            XLog log = eachLog.getValue();
            for (XTrace trace : log) {
                List<String> appear = new ArrayList<>();
                for (XEvent event : trace) {
                    String eventName = event.getAttributes().get("concept:name").toString();
                    String resourceName = event.getAttributes().get("org:resource").toString();
                    Map<String, Double> currentValue = orgTable.get(resourceName);
                    currentValue.put(eventName, currentValue.get(eventName) + 1);
                    orgTable.put(resourceName, currentValue);
                    actorTable.put(resourceName, actorTable.get(resourceName) + 1);
                }
            }
        }
    }

    private void computingFinalActorResourceValue(Map<String, Map<String, Double>> orgTable, Map<String, Integer> actorTable) {
        for (Map.Entry<String, Map<String, Double>> resource : orgTable.entrySet()) {
            Map<String, Double> events = resource.getValue();
            for (Map.Entry<String, Double> event : events.entrySet()) {
                events.put(event.getKey(), event.getValue() / actorTable.get(resource.getKey()));
            }
        }
    }

    private void computingFinalEventResourceValue(Map<String, Map<String, Double>> orgTable, Map<String, Integer> eventTable) {
        for (Map.Entry<String, Map<String, Double>> resource : orgTable.entrySet()) {
            for (Map.Entry<String, Integer> event : eventTable.entrySet()) {
                Map<String, Double> resourceEvent = resource.getValue();
                Double totalTaskPerActor = 0.0;
                for (Map.Entry<String, Double> task : resourceEvent.entrySet()) {
                    totalTaskPerActor += task.getValue();
                }
                resourceEvent.put(event.getKey(), resourceEvent.get(event.getKey()) / event.getValue());
            }
        }
    }

    private List<DoublePoint> transformOrgTableToListPoint(Map<String, Map<String, Double>> orgTable) {
        List<DoublePoint> pointList = new ArrayList<>();
        for (Map.Entry<String, Map<String, Double>> emp : orgTable.entrySet()) {
            List<Double> point = new ArrayList<>();
            for (Map.Entry<String, Double> task : emp.getValue().entrySet()) {
                point.add(task.getValue());
            }
            pointList.add(new DoublePoint(Stream.of(point.toArray(new Double[0])).mapToDouble(Double::doubleValue).toArray()));
        }
        return pointList;
    }

    private Map<String, double[]> transformOrgTableMapOfPoint(Map<String, Map<String, Double>> orgTable) {
        Map<String, double[]> pointList = new HashMap<>();
        for (Map.Entry<String, Map<String, Double>> emp : orgTable.entrySet()) {
            List<Double> point = new ArrayList<>();
            for (Map.Entry<String, Double> task : emp.getValue().entrySet()) {
                point.add(task.getValue());
            }
            pointList.put(emp.getKey(), Stream.of(point.toArray(new Double[0])).mapToDouble(Double::doubleValue).toArray());
        }
        return pointList;
    }

    private Map<String, List<String>> findOrganisationStructure(Map<String, Map<String, Double>> orgTable, Map<String, Map<String, Double>> orgTable2) {
        DBSCANClusterer clustering = new DBSCANClusterer(0.8, 0);
        List<DoublePoint> pointList = transformOrgTableToListPoint(orgTable);
        Map<String, double[]> actorPointList = transformOrgTableMapOfPoint(orgTable);
        List<Cluster<DoublePoint>> clusters = clustering.cluster(pointList);

        double minValueConsideredTask = 1 / (double) numberOfEvent;
        Map<String, List<String>> taskOfSpecificRole = new HashMap<>();
        int i = 1;
        for (Cluster<DoublePoint> cluster : clusters) {
            String groupName = "Role " + i;
            List<String> consideredTask = new ArrayList<>();
            for (DoublePoint point : cluster.getPoints()) {
                double[] clusteredPoint = point.getPoint();
                for (Map.Entry<String, double[]> actorPoint : actorPointList.entrySet()) {
                    if (Arrays.equals(clusteredPoint, actorPoint.getValue())) {
                        for (Map.Entry<String, Double> actorValue : orgTable2.get(actorPoint.getKey()).entrySet()) {
                            if (actorValue.getValue() > minValueConsideredTask) {
                                if (!consideredTask.contains(actorValue.getKey()))
                                    consideredTask.add(actorValue.getKey());
                            }
                        }
                    }
                }
            }
            taskOfSpecificRole.put(groupName, consideredTask);
            i++;
        }
        return taskOfSpecificRole;
    }

    private String writeProcessFragmentHeader(String group, String processName) {
        String content = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<ProcessFragments name=\"(0)\">\n" +
                "\t<ProcessFragment name=\"(1)\" version=\"\">\n" +
                "(*)" +
                "\t</ProcessFragment>\n" +
                "</ProcessFragments>";
        content = content.replace("(0)", group).replace("(1)", processName);
        return content;
    }

    private void writeProcessFragmentXML(String group, String content, String outputPath, String processName) {
        try {
            File file = new File(outputPath + "/" + processName);
            boolean value = file.mkdir();
            FileWriter myWriter = new FileWriter(outputPath + "/" + processName + "/" + group + ".xml");
            myWriter.write(content);
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeProcessFragment(Map<String, List<String>> taskOfSpecificRole, String outputPath, String processName) {
        for (Map.Entry<String, List<String>> group : taskOfSpecificRole.entrySet()) {
            String content = writeProcessFragmentHeader(group.getKey(), processName);
            StringBuilder activityContent = new StringBuilder();
            for (String task : group.getValue()) {
                String contentTask = "\t\t<Activity name=\"(0)\"> \n" +
                        "\t\t\t<MandatoryTask name=\"(0)\">\n" +
                        "(*)" +
                        "\t\t\t</MandatoryTask>\n" +
                        "\t\t</Activity>\n";
                StringBuilder artifactContent = new StringBuilder();
                for (Artifact artifact : mappingTaskArtifacts.get(task)) {
                    String tmp = "";
                    if (artifact.getTransition().equals("in")) {
                        tmp = "\t\t\t\t<InputArtifact name=\"(0)\" usage=\"ToStart\"/>\n";
                    } else if (artifact.getTransition().equals("out")) {
                        tmp = "\t\t\t\t<OutputArtifact name=\"(0)\" usage=\"ToFinish\"/>\n";
                    }
                    tmp = tmp.replace("(0)", artifact.getName());
                    artifactContent.append(tmp);
                }
                contentTask = contentTask.replace("(0)", task).replace("(*)", artifactContent.toString());
                activityContent.append(contentTask);
            }
            content = content.replace("(*)", activityContent);
            writeProcessFragmentXML(group.getKey(), content, outputPath, processName);
        }
    }

    private void loadingArtifact() {
        for (Map.Entry<String, XLog> eachLog : processLog.entrySet()) {
            XLog log = eachLog.getValue();
            XLogInfo info = XLogInfoFactory.createLogInfo(log);
            for (XTrace trace : log) {
                for (XEvent event : trace) {
                    String eventName = event.getAttributes().get("concept:name").toString();
                    XAttributeList artifactList = (XAttributeList) event.getAttributes().get("artifactlifecycle:moves");
                    List<Artifact> artifactListEvent = new ArrayList<>();
                    for (XAttribute artifact : artifactList.getCollection()) {
                        String artifactName = artifact.toString();
                        String artifactTrans = artifact.getAttributes().get("artifactlifecycle:transition").toString();
                        Artifact a1 = new Artifact(artifactName, artifactTrans);
                        artifactListEvent.add(a1);
                    }
                    if (!mappingTaskArtifacts.containsKey(eventName))
                        mappingTaskArtifacts.put(eventName, artifactListEvent);
                }
            }
        }
    }

    private void loadingArtifactMultiple() {
        for (Map.Entry<String, XLog> eachLog : processLog.entrySet()) {
            XLog log = eachLog.getValue();
            for (XTrace trace : log) {
                for (XEvent event : trace) {
                    String eventName = event.getAttributes().get("concept:name").toString();

                    if (mappingTaskArtifacts.containsKey(eventName)) {
                        mappingTaskArtifacts2.get(eventName).add(eachLog.getKey());
                    } else {
                        List<String> artifactList = new ArrayList<>();
                        artifactList.add(eachLog.getKey());
                        mappingTaskArtifacts2.put(eventName, artifactList);
                    }


                }
            }
        }
    }

    public void createProcessFragment(String inputFile, String processName) {
        XLog process = XESUtils.readXESFile(inputFile);
        processLog.put(processName, process);
        Object[] orgTableList = createOrgMatrix();
        Map<String, Map<String, Double>> orgTable1 = (Map<String, Map<String, Double>>) orgTableList[0];
//        System.out.println(orgTable1);
        Map<String, Map<String, Double>> orgTable2 = (Map<String, Map<String, Double>>) orgTableList[1];
        Map<String, List<String>> taskOfSpecificRole = findOrganisationStructure(orgTable1, orgTable2);
        loadingArtifact();
        String outputPath = "./resources/gen";
        writeProcessFragment(taskOfSpecificRole, outputPath, processName);
    }

    public void mcreateProcessFragmentFromMultiple(String path, String processName) {
        File directoryPath = new File(path);
        String[] files = directoryPath.list();

        for (int i = 0; i < Objects.requireNonNull(files).length; i++) {
            if (files[i].contains(".xes")) {
                XLog process = XESUtils.readXESFile(path + "/" + files[i]);
                String artifactName = files[i].substring(0, files[i].indexOf(".xes"));
                processLog.put(artifactName, process);
            }
        }
        Object[] orgTableList = createOrgMatrix();
        Map<String, Map<String, Double>> orgTable1 = (Map<String, Map<String, Double>>) orgTableList[0];
        Map<String, Map<String, Double>> orgTable2 = (Map<String, Map<String, Double>>) orgTableList[1];

        for(Map.Entry<String, Map<String, Double>> actorRow : orgTable1.entrySet()) {
            System.out.println(actorRow.getKey() + "\t" + actorRow.getValue());
        }

        Map<String, List<String>> taskOfSpecificRole = findOrganisationStructure(orgTable1, orgTable2);
        System.out.println(taskOfSpecificRole);
    }

    public Object[] createOrgMatrix() {
        List<XEventClass> eventClasses = new ArrayList<>();
        List<XEventClass> resourceClasses = new ArrayList<>();
        for (Map.Entry<String, XLog> eachLog : processLog.entrySet()) {
            XLog log = eachLog.getValue();
            XLogInfo info = XLogInfoFactory.createLogInfo(log);
            numberOfEvent = info.getEventClasses().size();
            addToListUnique(eventClasses, new ArrayList<>(info.getEventClasses().getClasses()));
            addToListUnique(resourceClasses, new ArrayList<>(info.getResourceClasses().getClasses()));
        }
        Object[] initTable = initialiseOrgTable(eventClasses, resourceClasses);
        Object[] initTable2 = initialiseActorTotal(eventClasses, resourceClasses);
        Map<String, Map<String, Double>> orgTable = (Map<String, Map<String, Double>>) initTable[0];
        Map<String, Integer> eventTable = (Map<String, Integer>) initTable[1];
        computingAppearance(orgTable, eventTable);
        computingFinalEventResourceValue(orgTable, eventTable);

        Map<String, Map<String, Double>> orgTable2 = (Map<String, Map<String, Double>>) initTable2[0];
        Map<String, Integer> actorTable = (Map<String, Integer>) initTable2[1];
        computingActorAppearance(orgTable2, actorTable);
        computingFinalActorResourceValue(orgTable2, actorTable);

        return new Object[]{orgTable, orgTable2};
    }

    private void addToListUnique(List<XEventClass> a, List<XEventClass> b) {
        for (XEventClass item : b) {
            if (a.contains(item)) {
                continue;
            }
            a.add(item);
        }
    }
}
