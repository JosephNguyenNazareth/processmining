package selfdevelop.extend;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.SimpleTokenizer;
import simplenlg.features.Feature;
import simplenlg.features.NumberAgreement;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.Lexicon;
import simplenlg.phrasespec.NPPhraseSpec;
import simplenlg.realiser.english.Realiser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TermDetect {
    SimpleTokenizer tokenizer;
    POSModel posModel;
    POSTaggerME posTagger;
    DictionaryLemmatizer lemmatizer;
    ChunkerME chunker;

    String[] stopWordList;

    public TermDetect() {
        try {
            tokenizer = SimpleTokenizer.INSTANCE;
            InputStream inputStreamPOSTagger = Files.newInputStream(Paths.get("/opennlp/models/en-pos-maxent.bin"));
            posModel = new POSModel(inputStreamPOSTagger);
            posTagger = new POSTaggerME(posModel);
            InputStream dictLemmatizer = Files.newInputStream(Paths.get("/opennlp/models/en-lemmatizer.dict"));
            lemmatizer = new DictionaryLemmatizer(dictLemmatizer);

            BufferedReader reader = new BufferedReader(new FileReader("/opennlp/models/en-stopwords.dict"));
            String stopwords = reader.readLine();
            stopWordList = stopwords.split(",");

            InputStream inputStreamChunker = Files.newInputStream(Paths.get("/opennlp/models/en-chunker.bin"));
            ChunkerModel chunkerModel
                    = new ChunkerModel(inputStreamChunker);
            chunker = new ChunkerME(chunkerModel);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSingularForm(String term) {
        Lexicon lexicon = Lexicon.getDefaultLexicon();

        NLGFactory nlgFactory = new NLGFactory(lexicon);
        Realiser realiser = new Realiser(lexicon);

        NPPhraseSpec subject = nlgFactory.createNounPhrase(term.toLowerCase());
        subject.setFeature(Feature.NUMBER, NumberAgreement.SINGULAR);
        return realiser.realiseSentence(subject).toLowerCase().replace(".","");
    }

    public Map<String, String> detectLemmaEnglish(String sentence) {
        sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", " ");
//        System.out.println(sentence);

        String[] tokens = tokenizer.tokenize(sentence);
        List<String> stopWordList2 = Arrays.asList(stopWordList);
        List<String> filteredTokensList = new ArrayList<>();

        for (String word:tokens) {
            if (!stopWordList2.contains(word))
                filteredTokensList.add(word);
        }

        String[] filteredTokens = filteredTokensList.toArray(new String[0]);
        String[] tags = posTagger.tag(filteredTokens);
        String[] lemmas = lemmatizer.lemmatize(filteredTokens, tags);

        System.out.println(Arrays.asList(tokens));
        System.out.println(Arrays.asList(filteredTokens));
        System.out.println(Arrays.asList(tags));
        System.out.println(Arrays.asList(lemmas));

        Map<String, String> tokenTag = new HashMap<>();
        for (int i = 0; i < tags.length; i++) {
            if (tags[i].contains("NN")) {
                String chosenTerm = "";
                if (lemmas[i].contains("O"))
                    chosenTerm = filteredTokens[i];
                else
                    chosenTerm = lemmas[i];
                String finalTerm = this.getSingularForm(chosenTerm);
                tokenTag.put(finalTerm, "noun");
            }
            else if (tags[i].contains("VB"))
                tokenTag.put(lemmas[i], "verb");
        }

        System.out.println(tokenTag);
        return tokenTag;
    }

    public Map<String, String> detectLemmaEnglish2(String sentence) {
        // remove special character
        sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", " ");

        // tokenizer
        String[] tokens = tokenizer.tokenize(sentence);

        // tagging part of sentence
        String[] tags = posTagger.tag(tokens);

        // find the base form of the verb using lammatizer
        // and we replace the vern in the original sentence with the base form verb
        String[] lemmas = lemmatizer.lemmatize(tokens, tags);
        for (int i = 0; i < tokens.length; i++) {
            if (!lemmas[i].equals("O")) {
                String singular = lemmas[i];
                if (tags[i].contains("NN"))
                     singular = this.getSingularForm(lemmas[i]);
                sentence = sentence.replace(tokens[i], singular);
            }
        }

        // we find the meaningful phrase in the sentence
        String[] chunks = chunker.chunk(tokens, tags);

//        System.out.println(sentence);
//        System.out.println(Arrays.asList(tokens));
//        System.out.println(Arrays.asList(tags));
//        System.out.println(Arrays.asList(lemmas));
//        System.out.println(Arrays.asList(chunks));

        // phrasal verb detection
        for (int i = 0; i < tokens.length - 1; i++) {
            if (tags[i].contains("V")) {
                if (tags[i + 1].contains("RP") || tags[i + 1].contains("IN") || tags[i + 1].contains("RB"))
                    if (chunks[i + 1].contains("B-"))
                        chunks[i + 1] = chunks[i + 1].replace("B","I");
            }
        }
//        System.out.println(Arrays.asList(chunks));

        // connect tokens in the same meaningful phrase together
        List<String> filteredTokensList = new ArrayList<>();
        List<String> filteredTokensTag = new ArrayList<>();
        int counter = -1;
        String tmp = "";
        String tmpTag = "";

        for (int i = 0; i < tokens.length; i++) {
            if (chunks[i].contains("B")) {
                if (counter >= 0) {
                    filteredTokensList.add(tmp);
                    filteredTokensTag.add(tmpTag);
                }
                tmp = tokens[i]; tmpTag = tags[i];
                counter++;
            } else if (chunks[i].contains("I")) {
                tmp += " " + tokens[i];
                tmpTag += " " + tags[i];
            }
        }
        filteredTokensList.add(tmp); filteredTokensTag.add(tmpTag);

        // tagging the terms found as a vern or a noun phrase
        Map<String, String> tokenTag = new HashMap<>();
        for (int i = 0; i < filteredTokensTag.size(); i++) {
            if (filteredTokensTag.get(i).contains("NN")) {
                tokenTag.put(filteredTokensList.get(i), "noun");
            }
            else if (filteredTokensTag.get(i).contains("VB"))
                tokenTag.put(filteredTokensList.get(i), "verb");
        }
        return tokenTag;
    }
}
