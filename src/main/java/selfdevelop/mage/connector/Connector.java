//package selfdevelop.mage.connector;
//
//import selfdevelop.casestudy.CaseStudy;
//import type.ProcessInstance;
//
//public interface Connector {
//    ProcessInstance connectProcessInstance(String processType, String processId, String user, String userRealName, String repoOrigin, String token, String repoLink, String repoDir);
//    ProcessInstance createProcessInstance(String creatorRole, String processType, String user, String userRealName, String repoOrigin, String token, String repoLink, String repoDir);
//    CaseStudy getCaseStudyMage();
//}