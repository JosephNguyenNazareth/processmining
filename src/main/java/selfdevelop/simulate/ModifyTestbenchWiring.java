package selfdevelop.simulate;

import org.apache.commons.lang3.ArrayUtils;

import java.time.LocalDateTime;
import java.util.*;

public class ModifyTestbenchWiring extends Simulation{
    protected int numberAlt = 0;
    private List<String[]> createTaskListMTW() {
        List<String[]> chance = new ArrayList<String[]>();
        chance.add(new String[]{"a","d", "i"});
        chance.add(new String[]{"b","e"});
        chance.add(new String[]{"j"});
        chance.add(new String[]{"c","f","g","k"});
        chance.add(new String[]{"h"});
        chance.add(new String[]{"l"});

        return chance;
    }

    private Map<String, Boolean> createTaskConditionMTW() {
        Map<String, Boolean> condition = new HashMap<String, Boolean>();
        condition.put("h", false);
        condition.put("j", false);
        condition.put("k", false);
        condition.put("l", false);

        return condition;
    }

    protected List<String> ModifyTestbenchWiringGen() {
        Random rand = new Random();
        List<String> instance = new ArrayList<String>();
        List<String[]> chance = createTaskListMTW();
        Map<String, Boolean> condition = createTaskConditionMTW();

        while (chance.size() > 0) {
            int nextSelection = rand.nextInt(chance.size());
            String[] chosen = chance.get(nextSelection);
            if (chosen[0].equals("h") && instance.toString().contains("f"))
                condition.put("h", true);
            if (chosen[0].equals("j") && instance.toString().contains("e") && instance.toString().contains("i"))
                condition.put("j", true);
            if (chosen[0].equals("k") && instance.toString().contains("g") && instance.toString().contains("h"))
                condition.put("k", true);
            if (chosen[0].equals("l") && instance.toString().contains("j") && instance.toString().contains("k"))
                condition.put("l", true);

            if ((!chosen[0].equals("h") && !chosen[0].equals("j") && !chosen[0].equals("k") && !chosen[0].equals("l")) || condition.get("h") || condition.get("j") || condition.get("k") || condition.get("l")) {
                instance.add(chosen[0]);
                if (condition.containsKey(chosen[0]))
                    condition.put(chosen[0], false);
                chosen = ArrayUtils.remove(chosen, 0);
                chance.remove(nextSelection);
                if (chosen.length > 0)
                    chance.add(chosen);
            }
        }
        return instance;
    }

    private String createTaskLine(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping) {
        Map<String, String> mapTaskActor = (Map<String, String>) taskMapping[0];
        Map<String, String> mapTaskRole = (Map<String, String>) taskMapping[1];
        StringBuilder content = new StringBuilder();
        content.append("\t<trace>\n");
        LocalDateTime localTime = LocalDateTime.now();
        for (String taskCode : taskSchedule) {
            String eventInfoStart = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<string key=\"org:role\" value=\"(1)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(3)\"/>\n" +
                    "\t\t\t<string key=\"lifecycle:transition\" value=\"start\"/>\n" +
                    "\t\t</event>\n";
            eventInfoStart = eventInfoStart.replace("(0)", mapTaskActor.get(taskCode)).replace("(1)", mapTaskRole.get(taskCode)).replace("(2)", localTime.toString()).replace("(3)", taskMappingCodeName.get(taskCode));
            content.append(eventInfoStart);
            localTime = localTime.plusHours(6);
            String eventInfoComplete = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<string key=\"org:role\" value=\"(1)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(3)\"/>\n" +
                    "\t\t\t<string key=\"lifecycle:transition\" value=\"complete\"/>\n" +
                    "\t\t</event>\n";
            eventInfoComplete = eventInfoComplete.replace("(0)", mapTaskActor.get(taskCode)).replace("(1)", mapTaskRole.get(taskCode)).replace("(2)", localTime.toString()).replace("(3)", taskMappingCodeName.get(taskCode));
            content.append(eventInfoComplete);
            localTime = localTime.plusHours(6);
        }
        content.append("\t</trace>\n");

        return content.toString();
    }

    private String createTaskLine2(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping) {
        Map<String, String> mapTaskActor = (Map<String, String>) taskMapping[0];
        Map<String, String> mapTaskRole = (Map<String, String>) taskMapping[1];
        StringBuilder content = new StringBuilder();
        content.append("\t<trace>\n");
        LocalDateTime localTime = LocalDateTime.now();
        for (String taskCode : taskSchedule) {
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<string key=\"org:role\" value=\"(1)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(3)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", mapTaskActor.get(taskCode)).replace("(1)", mapTaskRole.get(taskCode)).replace("(2)", localTime.toString()).replace("(3)", taskMappingCodeName.get(taskCode));
            content.append(eventInfo);
            localTime = localTime.plusHours(12);
        }
        content.append("\t</trace>\n");

        return content.toString();
    }

    protected String createArtifactInfo(Map<String, List<List<String>>> artifactMapping, LocalDateTime localTime, String taskCode) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";
        StringBuilder artifactEntries = new StringBuilder();
        for (List<String> artifact : artifactMapping.get(taskCode)) {
            String tmp = artifactMove.replace("(0)", artifact.get(0)).replace("(1)", artifact.get(0) + "_" + localTime).replace("(2)", artifact.get(1));
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());
        return artifactDescription;
    }

    protected String createTaskLineArtifactCentric(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping, Map<String, List<List<String>>> artifactMapping) {
        Map<String, String> mapTaskActor = (Map<String, String>) taskMapping[0];
        Map<String, String> mapTaskRole = (Map<String, String>) taskMapping[1];
        StringBuilder content = new StringBuilder();
        content.append("\t<trace>\n");
        LocalDateTime localTime = LocalDateTime.now();
        for (String taskCode : taskSchedule) {
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<string key=\"org:role\" value=\"(1)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(3)\"/>\n" +
                    "(4)" +
                    "\t\t</event>\n";
            if (!mapTaskActor.get(taskCode).equals("")) {
                eventInfo = eventInfo.replace("(0)", mapTaskActor.get(taskCode)).replace("(1)", mapTaskRole.get(taskCode)).replace("(2)", localTime.toString()).replace("(3)", taskMappingCodeName.get(taskCode));
                eventInfo = eventInfo.replace("(4)", createArtifactInfo(artifactMapping, localTime, taskCode));
                content.append(eventInfo);
            }
            localTime = localTime.plusHours(12);
        }
        content.append("\t</trace>\n");

        return content.toString();
    }

    protected Map<String, List<List<String>>> mapTaskArtifactMTW() {
        Map<String, List<List<String>>> mapTaskArtifact = new HashMap<>();
        mapTaskArtifact.put("a", Arrays.asList(Arrays.asList("Wiring Change Demand","in","defined"), Arrays.asList("Component Requirements","out","defined")));
        mapTaskArtifact.put("b", Arrays.asList(Arrays.asList("Wiring Change Demand","in","defined"), Arrays.asList("FICD","in","defined"), Arrays.asList("Electrical Specification","out","defined")));
        mapTaskArtifact.put("c", Arrays.asList(Arrays.asList("Wiring Change Demand","in","defined"), Arrays.asList("FICD","out","outlined")));
        mapTaskArtifact.put("d", Arrays.asList(Arrays.asList("Component Requirements","in","defined"), Arrays.asList("Components","out","outlined")));
        mapTaskArtifact.put("e", Arrays.asList(Arrays.asList("Electrical Specification","in","defined"), Arrays.asList("Electrical Design Model","out","defined")));
        mapTaskArtifact.put("f", Arrays.asList(Arrays.asList("FICD","in","outlined"), Arrays.asList("FICD","out","defined")));
        mapTaskArtifact.put("g", Arrays.asList(Arrays.asList("FICD","in","defined"), Arrays.asList("Bench Specification","out","defined")));
        mapTaskArtifact.put("h", Arrays.asList(Arrays.asList("FICD","in","defined"), Arrays.asList("ICD","out","defined")));
        mapTaskArtifact.put("i", Arrays.asList(Arrays.asList("Components","in","defined"), Arrays.asList("Installed Components","out","defined")));
        mapTaskArtifact.put("j", Arrays.asList(Arrays.asList("Components","in","defined"), Arrays.asList("Testbench Wired","out","defined")));
        mapTaskArtifact.put("k", Arrays.asList(Arrays.asList("Bench Specification","in","defined"), Arrays.asList("ICD","in","defined"), Arrays.asList("Testbench Configuration","out","defined")));
        mapTaskArtifact.put("l", Arrays.asList(Arrays.asList("Testbench Configuration","in","defined"), Arrays.asList("Testbench Wired","out","validated")));

        return mapTaskArtifact;
    }

    protected boolean alternativeMapTaskActorMTW(Map<String, String> mapTaskActor, List<String> taskSchedule) {
        Random rand = new Random();
        double alternativeTaskValue = 0.05;
        double skipTaskValue = 1 - alternativeTaskValue;
        double value = rand.nextDouble();
        if (value < alternativeTaskValue) {
            List<String> actors = Arrays.asList("A1", "A2","S1","ED1","T1", "T2", "T3", "T4","WT1", "WT2", "WT3","BC1", "BC2");
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            String altActor = actors.get(rand.nextInt(actors.size()));
            String altTask = tasks.get(rand.nextInt(tasks.size()));
            mapTaskActor.put(altTask, altActor);
            return true;
        } else if (value > skipTaskValue) {
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            String altTask = tasks.get(rand.nextInt(tasks.size()));
            mapTaskActor.put(altTask, "");
            return true;
        } else if (value > alternativeTaskValue && value < 1.2 * alternativeTaskValue) {
            int duplicateTask = rand.nextInt(4);
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            int altTaskPos = rand.nextInt(tasks.size());
            int pos0 = taskSchedule.indexOf(tasks.get(altTaskPos));
            int pos = pos0 + duplicateTask;
            if (pos < taskSchedule.size()) {
                taskSchedule.add(pos, tasks.get(altTaskPos));
                for (int i = 1; i < duplicateTask; i++) {
                    taskSchedule.add(pos + i, taskSchedule.get(pos0 + i));
                }
            } else {
                taskSchedule.add(tasks.get(altTaskPos));
                for (int i = 1; i < duplicateTask; i++) {
                    taskSchedule.add(taskSchedule.get(pos0 + i));
                }
            }
            System.out.println(taskSchedule);
            return true;
        }
        return false;
    }

    protected Object[] mapTaskActorMTW(List<String> taskSchedule) {
        Map<String, String> mapTaskActor = new HashMap<>();
        Map<String, String> mapTaskRole = new HashMap<>();
        mapTaskRole.put("a", "Analyst");
        mapTaskRole.put("b", "Analyst");
        mapTaskRole.put("c", "Analyst");
        mapTaskRole.put("d", "Supplier");
        mapTaskRole.put("e", "Electrical Designer");
        mapTaskRole.put("f", "Instrumentation Team");
        mapTaskRole.put("g", "Instrumentation Team");
        mapTaskRole.put("h", "Instrumentation Team");
        mapTaskRole.put("i", "Wiring Team");
        mapTaskRole.put("j", "Wiring Team");
        mapTaskRole.put("k", "Bench Coordinator");
        mapTaskRole.put("l", "Bench Coordinator");

        List<String> analystList = Arrays.asList("A1", "A2");
        List<String> supplierList = Arrays.asList("S1");
        List<String> electricalDesignerList = Arrays.asList("ED1");
        List<String> instrumentationTeamList = Arrays.asList("T1", "T2", "T3", "T4");
        List<String> wiringTeamList = Arrays.asList("WT1", "WT2", "WT3");
        List<String> benchCoordinatorList = Arrays.asList("BC1", "BC2");

        Random rand = new Random();
        String candidateAnalyst = analystList.get(rand.nextInt(analystList.size()));
        mapTaskActor.put("a", candidateAnalyst);
        mapTaskActor.put("b", candidateAnalyst);
        mapTaskActor.put("c", candidateAnalyst);
        mapTaskActor.put("d", supplierList.get(rand.nextInt(supplierList.size())));
        mapTaskActor.put("e", electricalDesignerList.get(rand.nextInt(electricalDesignerList.size())));
        String candidateIT = instrumentationTeamList.get(rand.nextInt(instrumentationTeamList.size()));
        mapTaskActor.put("f", candidateIT);
        mapTaskActor.put("g", candidateIT);
        mapTaskActor.put("h", candidateIT);
        String candidateWiringTeam = wiringTeamList.get(rand.nextInt(wiringTeamList.size()));
        mapTaskActor.put("i", candidateWiringTeam);
        mapTaskActor.put("j", candidateWiringTeam);
        String candidateBC = benchCoordinatorList.get(rand.nextInt(benchCoordinatorList.size()));;
        mapTaskActor.put("k", candidateBC);
        mapTaskActor.put("l", candidateBC);
        numberAlt = alternativeMapTaskActorMTW(mapTaskActor, taskSchedule) ? numberAlt + 1 : numberAlt;
        numberAlt = alternativeMapTaskActorMTW(mapTaskActor, taskSchedule) ? numberAlt + 1 : numberAlt;

        return new Object[]{mapTaskActor, mapTaskRole};
    }

    public void genProcessInstance(int nbProc) {
        String processName0 = "Modify Testbench Wiring";
        String processName = "Modify Testbench Wiring 0.05x2_2";
        String path = "./resources/Description.xml";
        Map<String, String> taskMappingCodeName = readXML(processName0, path);
        StringBuilder fileContent = new StringBuilder(headerXES(processName));
        StringBuilder taskScheduleList = new StringBuilder();
        Map<String, List<List<String>>> mapTaskArtifact = mapTaskArtifactMTW();
        for (int i = 0; i < nbProc; i++) {
            List<String> taskSchedule = ModifyTestbenchWiringGen();
            Object[] mappingTask = mapTaskActorMTW(taskSchedule);
            taskScheduleList.append(taskSchedule).append("\n");
            String content = createTaskLineArtifactCentric(taskSchedule, taskMappingCodeName, mappingTask, mapTaskArtifact);
            fileContent.append(content);
        }
        fileContent.append(footerXES());
        System.out.println(numberAlt);
        saveFile(fileContent.toString(), processName, "./example-logs");
        saveTaskOrder(taskScheduleList.toString(), processName, "./example-logs");
    }
}
