package selfdevelop.simulate;

import java.time.LocalDateTime;
import java.util.*;

public class RequestProcessing extends Simulation{
    private Object[] mapTaskActorRP(List<String> taskSchedule) {
        List<List<String>> mapTaskActor = new ArrayList<>();
        Map<String, String> mapTaskRole = new HashMap<>();

        Map<String, List<String>> mapRoleActor = new HashMap<>();
        List<String> assistantList = Arrays.asList("Mike", "Ellen", "Pete");
        List<String> expertList = Arrays.asList("Sue", "Sean");
        List<String> managerList = Arrays.asList("Sara");
        mapRoleActor.put("Assistant", assistantList);
        mapRoleActor.put("Expert", expertList);
        mapRoleActor.put("Manager", managerList);

        mapTaskRole.put("a", "Assistant");
        mapTaskRole.put("b", "Expert");
        mapTaskRole.put("c", "Assistant");
        mapTaskRole.put("d", "Assistant");
        mapTaskRole.put("e", "Manager");
        mapTaskRole.put("f", "Manager");
        mapTaskRole.put("g", "Assistant");
        mapTaskRole.put("h", "Assistant");

        Random rand = new Random();
        for (String task: taskSchedule) {
            List<String> candidate = mapRoleActor.get(mapTaskRole.get(task));
            int nextSelection = rand.nextInt(candidate.size());
            List<String> taskActor = Arrays.asList(task, candidate.get(nextSelection));
            mapTaskActor.add(taskActor);
        }

        return new Object[]{mapTaskActor, mapTaskRole};
    }

    private List<String> strToListStr(String task) {
        List<String> taskList = new ArrayList<>();
        for (int i = 0; i < task.length(); i++) {
            taskList.add(Character.toString(task.charAt(i)));
        }
        return taskList;
    }

    private String createTaskLine2(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping) {
        List<List<String>> mapTaskActor = (List<List<String>>) taskMapping[0];
        Map<String, String> mapTaskRole = (Map<String, String>) taskMapping[1];
        StringBuilder content = new StringBuilder();
        content.append("\t<trace>\n");
        LocalDateTime localTime = LocalDateTime.now();
        int i = 0;
        for (String taskCode : taskSchedule) {
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<string key=\"org:role\" value=\"(1)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(3)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", mapTaskActor.get(i).get(1)).replace("(1)", mapTaskRole.get(taskCode)).replace("(2)", localTime.toString()).replace("(3)", taskMappingCodeName.get(taskCode));
            content.append(eventInfo);
            localTime = localTime.plusHours(12);
            i++;
        }
        content.append("\t</trace>\n");

        return content.toString();
    }

    public void genProcessInstance(int nbProc)  {
        String regex = "a(((b|c)d)|(d(b|c)))e(f(((b|c)d)|(d(b|c)))e){0,2}(g|h)";
        String processName0 = "Request Processing";
        String processName = "Request Processing 2";

        String path = "./resources/Description.xml";
        Map<String, String> taskMappingCodeName = readXML(processName0, path);
        StringBuilder fileContent = new StringBuilder(headerXES(processName));
        StringBuilder taskScheduleList = new StringBuilder();
        for (int i = 0; i < nbProc; i++) {
            List<String> taskSchedule = strToListStr(generateTextFromRegex(regex));
            Object[] mappingTask = mapTaskActorRP(taskSchedule);
            taskScheduleList.append(mappingTask[0]).append("\n");
            String content = createTaskLine2(taskSchedule, taskMappingCodeName, mappingTask);
            fileContent.append(content);
        }
        fileContent.append(footerXES());
        saveFile(fileContent.toString(), processName, "./example-logs");
        saveTaskOrder(taskScheduleList.toString(), processName, "./example-logs");
    }
}
