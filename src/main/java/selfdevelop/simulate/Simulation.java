package selfdevelop.simulate;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import nl.flotsam.xeger.Xeger;
import org.bson.Document;
import selfdevelop.type.GenerateXML;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Simulation {
    protected String generateTextFromRegex(String regex) {
        Xeger generator = new Xeger(regex);
        List<String> result = new ArrayList<String>();
        String text = generator.generate();
        assert text.matches(regex);
        return text;
    }

    protected String headerXES(String processName) {
        String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<!-- This file has been generated with the OpenXES library. It conforms -->\n" +
                "<!-- to the XML serialization of the XES standard for log storage and -->\n" +
                "<!-- management. -->\n" +
                "<!-- XES standard version: 1.0 -->\n" +
                "<!-- OpenXES library version: 1.0RC7 -->\n" +
                "<!-- OpenXES is available from http://www.openxes.org/ -->\n" +
                "<log xes.version=\"1.0\" xes.features=\"nested-attributes\" openxes.version=\"1.0RC7\" xmlns=\"http://www.xes-standard.org/\">\n" +
                "\t<extension name=\"Lifecycle\" prefix=\"lifecycle\" uri=\"http://www.xes-standard.org/lifecycle.xesext\"/>\n" +
                "\t<extension name=\"Organizational\" prefix=\"org\" uri=\"http://www.xes-standard.org/org.xesext\"/>\n" +
                "\t<extension name=\"Time\" prefix=\"time\" uri=\"http://www.xes-standard.org/time.xesext\"/>\n" +
                "\t<extension name=\"Concept\" prefix=\"concept\" uri=\"http://www.xes-standard.org/concept.xesext\"/>\n" +
                "\t<extension name=\"Semantic\" prefix=\"semantic\" uri=\"http://www.xes-standard.org/semantic.xesext\"/>\n" +
                "\t<extension name=\"ArtifactLifecycle\" prefix=\"artifactlifecycle\" uri=\"http://xes-standard.org/artifactlifecycle.xesext\"/>\n\n" +
                "\t<global scope=\"trace\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t</global>\n" +
                "\t<global scope=\"event\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t\t<string key=\"lifecycle:transition\" value=\"complete\"/>\n" +
                "\t</global>\n" +
                "\t<classifier name=\"MXML Legacy Classifier\" keys=\"concept:name lifecycle:transition\"/>\n" +
                "\t<classifier name=\"Event Name\" keys=\"concept:name\"/>\n" +
                "\t<classifier name=\"Resource\" keys=\"org:resource\"/>\n" +
                "\t<string key=\"source\" value=\"Rapid Synthesizer\"/>\n" +
                "\t<string key=\"concept:name\" value=\"(0).mxml\"/>\n" +
                "\t<string key=\"lifecycle:model\" value=\"standard\"/>\n";
        header = header.replace("(0)", processName);
        return header;
    }

    protected String headerXES(String processName, String resource) {
        String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<!-- This file has been generated with the OpenXES library. It conforms -->\n" +
                "<!-- to the XML serialization of the XES standard for log storage and -->\n" +
                "<!-- management. -->\n" +
                "<!-- XES standard version: 1.0 -->\n" +
                "<!-- OpenXES library version: 1.0RC7 -->\n" +
                "<!-- OpenXES is available from http://www.openxes.org/ -->\n" +
                "<log xes.version=\"1.0\" xes.features=\"nested-attributes\" openxes.version=\"1.0RC7\" xmlns=\"http://www.xes-standard.org/\">\n" +
                "\t<extension name=\"Lifecycle\" prefix=\"lifecycle\" uri=\"http://www.xes-standard.org/lifecycle.xesext\"/>\n" +
                "\t<extension name=\"Organizational\" prefix=\"org\" uri=\"http://www.xes-standard.org/org.xesext\"/>\n" +
                "\t<extension name=\"Time\" prefix=\"time\" uri=\"http://www.xes-standard.org/time.xesext\"/>\n" +
                "\t<extension name=\"Concept\" prefix=\"concept\" uri=\"http://www.xes-standard.org/concept.xesext\"/>\n" +
                "\t<extension name=\"Semantic\" prefix=\"semantic\" uri=\"http://www.xes-standard.org/semantic.xesext\"/>\n" +
                "\t<extension name=\"ArtifactLifecycle\" prefix=\"artifactlifecycle\" uri=\"http://xes-standard.org/artifactlifecycle.xesext\"/>\n\n" +
                "\t<global scope=\"trace\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t</global>\n" +
                "\t<global scope=\"event\">\n" +
                "\t\t<string key=\"concept:name\" value=\"__INVALID__\"/>\n" +
                "\t\t<string key=\"lifecycle:transition\" value=\"complete\"/>\n" +
                "\t</global>\n" +
                "\t<classifier name=\"MXML Legacy Classifier\" keys=\"concept:name lifecycle:transition\"/>\n" +
                "\t<classifier name=\"Event Name\" keys=\"concept:name\"/>\n" +
                "\t<classifier name=\"Resource\" keys=\"org:resource\"/>\n" +
                "\t<string key=\"source\" value=\"Rapid Synthesizer\"/>\n" +
                "\t<string key=\"concept:name\" value=\"(0).mxml\"/>\n" +
                "\t<string key=\"lifecycle:model\" value=\"standard\"/>\n";
        header = header.replace("(0)", processName + "_" + resource);
        return header;
    }

    protected String footerXES() {
        return "</log>";
    }

    protected Map<String, String> readXML(String processName, String descriptionFile) {
        GenerateXML xmlFile = new GenerateXML(descriptionFile);
        List<String> resultTask  = xmlFile.analyse(processName, "Task", "name");
        List<String> resultTaskCode  = xmlFile.analyse(processName, "Task", "code");
        Map<String, String> taskMappingCodeName = new HashMap<String, String>();

        for (int i = 0; i < resultTask.size(); i++) {
            taskMappingCodeName.put(resultTaskCode.get(i), resultTask.get(i));
        }

        return taskMappingCodeName;
    }

    protected void saveFile(String content, String processName, String path) {
        try {
            File savePath = new File(path);
            if (!savePath.isDirectory()) {
                savePath.mkdir();
            }

            FileWriter myWriter = new FileWriter(path + "/" + processName + ".xes");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Written successfully");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    protected void saveFile(String content, String processName, String source, String path) {
        try {
            File savePath = new File(path);
            if (!savePath.isDirectory()) {
                savePath.mkdir();
            }

            if (source.contains("/")) {
                source = source.replace("/", "_");
            }

            FileWriter myWriter = new FileWriter(path + "/" + processName +  "_" + source + ".xes");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Written successfully");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    protected void saveTaskOrder(String content, String processName, String path) {
        try {
            File savePath = new File(path);
            if (!savePath.isDirectory()) {
                savePath.mkdir();
            }

            FileWriter myWriter = new FileWriter(path + "/" + processName + ".txt");
            myWriter.write(content);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public MongoCollection<Document> connectMongoDB(String dbName, String collection) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        return database.getCollection(collection);
    }

    public List<Document> getMongoDBDocuments(String dbName, String collection, BasicDBObject query) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);
        MongoCollection<Document> collectionFound = database.getCollection(collection);
        List<Document> result = new ArrayList<>();

        for (Document doc : collectionFound.find(query)) {
            result.add(doc);
        }

        return result;
    }

//    public void getDocumentsFromProjectName(String dbName, String collection, String field, String value) {
//        MongoClient client = MongoClients.create("mongodb://localhost:27017");
//        MongoDatabase database = client.getDatabase(dbName);
//
//        MongoCollection<Document> collectionData =  database.getCollection(collection);
//
//        BasicDBObject query = new BasicDBObject();
//        query.put(field, new BasicDBObject("$eq", value));
//
////        return collectionData.find(query).;
//    }
}
