package selfdevelop.simulate;

import selfdevelop.type.ArtifactLog;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

public class SimulationArtifact extends ModifyTestbenchWiring implements Runnable{
    public int getThreadNo() {
        return threadNo;
    }

    public void setThreadNo(int threadNo) {
        this.threadNo = threadNo;
    }

    private int threadNo;
    public int nbProc;
    public String processName0;
    public String processName;
    public ArtifactLog afLog;
    private final List<String> possibleSharedTask = Arrays.asList("e","i"); // just choose some tasks that can be shared between instances
    private final List<String> possiblePreSharedTask = Arrays.asList("b","d"); // just choose some tasks that can be shared between instances
    private String sharedTask;
    private String preSharedTask;
    private final List<String> referenceArtifact = new ArrayList<>();
    private final List<String> leftTask = new ArrayList<>();
    private int convergenceInstance = 0;
    private final Map<String, Integer> artifactStorage = new HashMap<>();
    private Thread t;

    public Integer getIdFromArtifactStorage(String artifactName) {
        return artifactStorage.get(artifactName);
    }

    public synchronized void updateArtifactStorage(String artifactName) {
        if (artifactStorage.containsKey(artifactName)) {
            artifactStorage.put(artifactName, artifactStorage.get(artifactName) + 1);
        } else {
            artifactStorage.put(artifactName, 0);
        }
    }

    protected void saveFile(String content, String processName, String path) {
        try {
            FileWriter myWriter = new FileWriter(path + "/" + processName + ".csv", true);
            myWriter.write(content);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    protected void saveNewFile(String content, String processName, String path) {
        try {
            FileWriter myWriter = new FileWriter(path + "/" + processName + ".csv");
            myWriter.write(content);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public void genProcessInstance(int nbProc) {
        processName0 = "Modify Testbench Wiring";
        processName = "Modify Testbench Wiring 0.05x2_2_new 2 threads";

        afLog = new ArtifactLog();
        saveNewFile(afLog.getTitle() + "\n", processName, "./example-logs");

        this.nbProc = nbProc;
        for (int i = 0; i < 2; i++) {
            t = new Thread(this);
            t.setName(String.valueOf(i));
            t.start();
        }
    }

    protected void createArtifactLog(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping, Map<String, List<List<String>>> artifactMapping, String path) {
        try {
            Random rand = new Random();
            Map<String, String> mapTaskActor = (Map<String, String>) taskMapping[0];
            LocalDateTime localTime = LocalDateTime.now();
            Map<String, String> mapArtifactType2Instance = new HashMap<>();
            for (String taskCode : taskSchedule) {
                for (List<String> artifact1 : artifactMapping.get(taskCode)) {
                    String artifactInType = artifact1.get(0);
                    if (artifact1.get(1).equals("in")) {
                        // this is for the very first artifact only
                        if (!mapArtifactType2Instance.containsKey(artifactInType)) {
                            mapArtifactType2Instance.put(artifactInType, artifactInType + "_" + localTime);
                        }

                        for (List<String> artifact : artifactMapping.get(taskCode)) {
                            if (artifact.get(1).equals("out")) {
                                String artifactId = artifact.get(0) + "_" + localTime;
                                if (convergenceInstance > 1 && taskCode.equals(preSharedTask)) {
                                    addReferenceArtifact(artifactId);
                                }
                                if (convergenceInstance == 1 && taskCode.equals(sharedTask)) {
                                    for (String refArtifactId : referenceArtifact) {
                                        StringBuilder content = new StringBuilder();
                                        content.append(localTime);
                                        content.append(",");
                                        content.append(artifactId);
                                        content.append(",");
                                        content.append(artifact.get(0));
                                        content.append(",");
                                        content.append(taskMappingCodeName.get(taskCode));
                                        content.append(",");
                                        content.append(mapTaskActor.get(taskCode));
                                        content.append(",");
                                        content.append(refArtifactId);
                                        content.append("\n");
                                        System.out.println(content);

                                        saveFile(content.toString(), processName, path);
                                        localTime = localTime.plusSeconds(rand.nextInt(3600 * 5 + 1) + 3600);
                                    }
                                    referenceArtifact.clear();
                                }

                                StringBuilder content = new StringBuilder();
                                content.append(localTime);
                                content.append(",");
                                content.append(artifactId);
                                content.append(",");
                                content.append(artifact.get(0));
                                content.append(",");
                                content.append(taskMappingCodeName.get(taskCode));
                                content.append(",");
                                content.append(mapTaskActor.get(taskCode));
                                content.append(",");
                                content.append(mapArtifactType2Instance.get(artifactInType));
                                content.append("\n");

                                // update the artifactId
                                mapArtifactType2Instance.put(artifact.get(0), artifactId);
                                Thread.sleep(15);
                                saveFile(content.toString(), processName, path);
                            }
                        }
                    }
                }
                localTime = localTime.plusSeconds(rand.nextInt(3600 * 5 + 1) + 3600);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void createArtifactLogNew(List<String> taskSchedule, Map<String, String> taskMappingCodeName, Object[] taskMapping, Map<String, List<List<String>>> artifactMapping, String path) {
        try {
            Random rand = new Random();
            Map<String, String> mapTaskActor = (Map<String, String>) taskMapping[0];
            LocalDateTime localTime = LocalDateTime.now();
            List<String> updatedArtifact = new ArrayList<>();
            for (String taskCode : taskSchedule) {
                for (List<String> artifact : artifactMapping.get(taskCode)) {
                    String artifactName = artifact.get(0);
                    if (!updatedArtifact.contains(artifactName)) {
                        updateArtifactStorage(artifactName);
                        updatedArtifact.add(artifactName);
                    }
                    String artifactId = artifactName + getIdFromArtifactStorage(artifactName);

                    // for multi instance, must add the reference artifact to a list in order to recall when necessary
                    if (convergenceInstance >= 1 && taskCode.equals(preSharedTask) && artifact.get(1).equals("out")) {
                        addReferenceArtifact(artifactId);
                    }
                    if (convergenceInstance == 1 && taskCode.equals(sharedTask)) {
                        if (artifact.get(1).equals("in")) {
                            System.out.println("ref" + referenceArtifact);
                            for (String refArtifactId : referenceArtifact) {
                                StringBuilder content = new StringBuilder();
                                content.append(localTime);
                                content.append(",");
                                content.append(refArtifactId);
                                content.append(",");
                                content.append(artifact.get(0));
                                content.append(",");
                                content.append("read");
                                content.append(",");
                                content.append("defined");
                                content.append(",");
                                content.append(taskMappingCodeName.get(taskCode));
                                content.append(",");
                                content.append(mapTaskActor.get(taskCode));
                                content.append("\n");
                                System.out.println(content);

                                saveFile(content.toString(), processName, path);
                                localTime = localTime.plusSeconds(rand.nextInt(3600 * 5 + 1) + 3600);
                            }
                            clearReferenceArtifact();
                            continue;
                        }
                    }

                    StringBuilder content = new StringBuilder();
                    content.append(localTime);
                    content.append(",");
                    content.append(artifactId);
                    content.append(",");
                    content.append(artifactName);
                    content.append(",");
                    if (artifact.get(1).equals("in")) {
                        content.append("read");
                        content.append(",");
                        if (!taskCode.equals("f"))
                            content.append("defined");
                        else
                            content.append("outlined");
                    } else {
                        if (!taskCode.equals("f"))
                            content.append("created");
                        else
                            content.append("modified");
                        content.append(",");
                        if (!taskCode.equals("c"))
                            content.append("defined");
                        else
                            content.append("outlined");
                    }
                    content.append(",");
                    content.append(taskMappingCodeName.get(taskCode));
                    content.append(",");
                    content.append(mapTaskActor.get(taskCode));
                    content.append("\n");
                    Thread.sleep(15);
                    saveFile(content.toString(), processName, path);
                }
                localTime = localTime.plusSeconds(rand.nextInt(3600 * 5 + 1) + 3600);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void altMapTaskActorMTW(Map<String, String> mapTaskActor, List<String> taskSchedule) {
        Random rand = new Random();
        double alternativeTaskValue = 0.05;
        double skipTaskValue = 1 - alternativeTaskValue;
        double value = rand.nextDouble();
        if (value < alternativeTaskValue) {
            List<String> actors = Arrays.asList("A1", "A2","S1","ED1","T1", "T2", "T3", "T4","WT1", "WT2", "WT3","BC1", "BC2");
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            String altActor = actors.get(rand.nextInt(actors.size()));
            String altTask = tasks.get(rand.nextInt(tasks.size()));
            mapTaskActor.put(altTask, altActor);
            convergenceInstance = 0;
        } else if (value > skipTaskValue) {
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            String altTask = tasks.get(rand.nextInt(tasks.size()));
            mapTaskActor.put(altTask, "");
            convergenceInstance = 0;
        } else if (value > alternativeTaskValue && value < 1.2 * alternativeTaskValue) {
            int duplicateTask = rand.nextInt(4);
            List<String> tasks = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l");
            int altTaskPos = rand.nextInt(tasks.size());
            int pos0 = taskSchedule.indexOf(tasks.get(altTaskPos));
            int pos = pos0 + duplicateTask;
            if (pos < taskSchedule.size()) {
                taskSchedule.add(pos, tasks.get(altTaskPos));
                for (int i = 1; i < duplicateTask; i++) {
                    taskSchedule.add(pos + i, taskSchedule.get(pos0 + i));
                }
            } else {
                taskSchedule.add(tasks.get(altTaskPos));
                for (int i = 1; i < duplicateTask; i++) {
                    taskSchedule.add(taskSchedule.get(pos0 + i));
                }
            }
            convergenceInstance = 0;
        } else if (value >= 1.2 * alternativeTaskValue && value < 1.5 * alternativeTaskValue) {
            convergenceInstance = rand.nextInt(4) + 1;
            int sharedTaskIndex = rand.nextInt(possibleSharedTask.size());
            sharedTask = possibleSharedTask.get(sharedTaskIndex);
            preSharedTask = possiblePreSharedTask.get(sharedTaskIndex);

            int found = -1;
            for (int i = 0; i < taskSchedule.size(); i++) {
                if (taskSchedule.get(i).equals(sharedTask)) {
                   found = i;
                   break;
                }
            }
            while (found < taskSchedule.size()) {
                leftTask.add(taskSchedule.get(found));
                taskSchedule.remove(found);
            }
        } else
            convergenceInstance = -1;
    }

    protected synchronized void addReferenceArtifact(String artifactId) {
        referenceArtifact.add(artifactId);
    }

    protected synchronized void clearReferenceArtifact() {
        referenceArtifact.clear();
    }

    protected Object[] mapTaskActorMTW(List<String> taskSchedule) {
        Map<String, String> mapTaskActor = new HashMap<>();
        Map<String, String> mapTaskRole = new HashMap<>();
        mapTaskRole.put("a", "Analyst");
        mapTaskRole.put("b", "Analyst");
        mapTaskRole.put("c", "Analyst");
        mapTaskRole.put("d", "Supplier");
        mapTaskRole.put("e", "Electrical Designer");
        mapTaskRole.put("f", "Instrumentation Team");
        mapTaskRole.put("g", "Instrumentation Team");
        mapTaskRole.put("h", "Instrumentation Team");
        mapTaskRole.put("i", "Wiring Team");
        mapTaskRole.put("j", "Wiring Team");
        mapTaskRole.put("k", "Bench Coordinator");
        mapTaskRole.put("l", "Bench Coordinator");

        List<String> analystList = Arrays.asList("A1", "A2");
        List<String> supplierList = Arrays.asList("S1");
        List<String> electricalDesignerList = Arrays.asList("ED1");
        List<String> instrumentationTeamList = Arrays.asList("T1", "T2", "T3", "T4");
        List<String> wiringTeamList = Arrays.asList("WT1", "WT2", "WT3");
        List<String> benchCoordinatorList = Arrays.asList("BC1", "BC2");

        Random rand = new Random();
        String candidateAnalyst = analystList.get(rand.nextInt(analystList.size()));
        mapTaskActor.put("a", candidateAnalyst);
        mapTaskActor.put("b", candidateAnalyst);
        mapTaskActor.put("c", candidateAnalyst);
        mapTaskActor.put("d", supplierList.get(rand.nextInt(supplierList.size())));
        mapTaskActor.put("e", electricalDesignerList.get(rand.nextInt(electricalDesignerList.size())));
        String candidateIT = instrumentationTeamList.get(rand.nextInt(instrumentationTeamList.size()));
        mapTaskActor.put("f", candidateIT);
        mapTaskActor.put("g", candidateIT);
        mapTaskActor.put("h", candidateIT);
        String candidateWiringTeam = wiringTeamList.get(rand.nextInt(wiringTeamList.size()));
        mapTaskActor.put("i", candidateWiringTeam);
        mapTaskActor.put("j", candidateWiringTeam);
        String candidateBC = benchCoordinatorList.get(rand.nextInt(benchCoordinatorList.size()));
        mapTaskActor.put("k", candidateBC);
        mapTaskActor.put("l", candidateBC);
        if (convergenceInstance < 1)
            altMapTaskActorMTW(mapTaskActor, taskSchedule);
        if (convergenceInstance < 1)
            altMapTaskActorMTW(mapTaskActor, taskSchedule);
        if (convergenceInstance == 1) {
            taskSchedule.addAll(leftTask);
        }

        return new Object[]{mapTaskActor, mapTaskRole};
    }


    public void run() {
        String path = "./resources/Description.xml";
        Map<String, String> taskMappingCodeName = readXML(processName0, path);
        StringBuilder taskScheduleList = new StringBuilder();
        Map<String, List<List<String>>> mapTaskArtifact = mapTaskArtifactMTW();
        List<String> taskSchedule = new ArrayList<>();
        for (int i = 0; i < nbProc; i++) {
            if (convergenceInstance < 1)
                taskSchedule = ModifyTestbenchWiringGen();
            Object[] mappingTask = mapTaskActorMTW(taskSchedule);
            System.out.println(taskSchedule.toString() + convergenceInstance);
            taskScheduleList.append(taskSchedule).append("\n");
            createArtifactLogNew(taskSchedule, taskMappingCodeName, mappingTask, mapTaskArtifact, "./example-logs");
            if (convergenceInstance >= 1)
                convergenceInstance--;
        }
        saveTaskOrder(taskScheduleList.toString(), processName + t.getName(), "./example-logs");
    }
}
