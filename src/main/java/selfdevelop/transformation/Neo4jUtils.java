//package selfdevelop.transformation;
//
//import io.javalin.http.Context;
//import org.neo4j.driver.AuthToken;
//import org.neo4j.driver.AuthTokens;
//import org.neo4j.driver.Driver;
//import org.neo4j.driver.GraphDatabase;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.util.List;
//import java.util.Map;
//
//public class Neo4jUtils {
//    public static void loadProperties() {
//        try {
//            InputStream file = Neo4jUtils.class.getResourceAsStream("/application.properties");
//            if (file!=null) System.getProperties().load(file);
//        } catch (IOException e) {
//            throw new RuntimeException("Error loading application.properties", e);
//        }
//    }
//
//    // tag::initDriver[]
//    public static Driver initDriver() {
//        AuthToken auth = AuthTokens.basic(getNeo4jUsername(), getNeo4jPassword());
//        Driver driver = GraphDatabase.driver(getNeo4jUri(), auth);
//        driver.verifyConnectivity();
//
//        return driver;
//    }
//    // end::initDriver[]
//
//    static int getServerPort() {
//        return Integer.parseInt(System.getProperty("APP_PORT", "3000"));
//    }
//
//    static String getJwtSecret() {
//        return System.getProperty("JWT_SECRET");
//    }
//
//    public static String getNeo4jUri() {
//        return System.getProperty("NEO4J_URI");
//    }
//    public static String getNeo4jUsername() {
//        return System.getProperty("NEO4J_USERNAME");
//    }
//    public static String getNeo4jPassword() {
//        return System.getProperty("NEO4J_PASSWORD");
//    }
//}
