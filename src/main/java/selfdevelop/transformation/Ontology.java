package selfdevelop.transformation;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.semanticweb.owlapi.search.Searcher.annotations;
import static org.semanticweb.owlapi.vocab.OWLRDFVocabulary.RDFS_LABEL;

public class Ontology {
    private final OWLOntologyManager manager;
    private final OWLOntology ontology;
    private final OWLDataFactory df;
    private final String ontologyIRI;
    private final String defaultDir;

    public Ontology(String defaultDir, String ontologyIRI) {
        this.manager = OWLManager.createOWLOntologyManager();
        try {
            this.ontology = this.manager.loadOntologyFromOntologyDocument(new File(defaultDir));
            this.ontologyIRI = ontologyIRI;
            this.defaultDir = defaultDir;
        } catch (OWLOntologyCreationException e) {
            throw new RuntimeException(e);
        }
        this.ontology.getAnnotationPropertiesInSignature();
        this.df = manager.getOWLDataFactory();
    }

    public void parseOntology() {
        for (OWLClass cls : ontology.getClassesInSignature()) {
            String id = cls.getIRI().toString();
            String label = get(cls, RDFS_LABEL.toString()).get(0);
            System.out.println(label + " [" + id + "]");
        }
    }

    public boolean findLabel(String labelOriginal) {
        String label = labelOriginal.toLowerCase();
        for (OWLClass cls : ontology.getClassesInSignature()) {
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                if (labelFound.contains(label) || label.contains(labelFound)) {
                    return true;
                }
            }
        }
        return false;
    }

    public OWLClass getExactTerm(String labelOriginal) {
        String label = labelOriginal.toLowerCase();
        Map<String, OWLClass> possibleMatch = new HashMap<>();

        for (OWLClass cls : ontology.getClassesInSignature()) {
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                if (labelFound.equals(label)) {
                    return cls;
                }
            }
        }
        return null;
    }

    public OWLClass getTerm(String labelOriginal) {
        String label = labelOriginal.toLowerCase();
        Map<String, OWLClass> possibleMatch = new HashMap<>();

        OWLClass exactlyMatch = null;
        OWLClass firstRelative = null;
        for (OWLClass cls : ontology.getClassesInSignature()) {
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                if (labelFound.contains(label) || label.contains(labelFound)) {
                    if (firstRelative == null)
                        firstRelative = cls;
                }
                if (labelFound.equals(label))
                    exactlyMatch = cls;
            }
        }

        if (exactlyMatch != null)
            return exactlyMatch;
        else
            return firstRelative;
    }

    private List<String> get(OWLClass clazz, String property) {
        List<String> ret = new ArrayList<>();

        final OWLAnnotationProperty owlProperty = df.getOWLAnnotationProperty(IRI.create(property));
        for (OWLOntology o : ontology.getImportsClosure()) {
            List<OWLAnnotation> annotationList = annotations(o.getAnnotationAssertionAxioms(clazz.getIRI()).stream(), owlProperty).collect(Collectors.toList());
            for (OWLAnnotation annotation : annotationList) {
                if (annotation.getValue() instanceof OWLLiteral) {
                    OWLLiteral val = (OWLLiteral) annotation.getValue();
                    ret.add(val.getLiteral());
                }
            }
        }
        return ret;
    }

    public void saveOntology() {
        File output = new File(this.defaultDir);
        IRI documentIRI2 = IRI.create(output);
        try {
            manager.saveOntology(ontology, new OWLXMLOntologyFormat(), documentIRI2);
        } catch (OWLOntologyStorageException e) {
            throw new RuntimeException(e);
        }
    }

    public void addTerm(String term, String termParent) {
        OWLClass classChild = df.getOWLClass(IRI.create(ontologyIRI + "#" + term));
        OWLClass classParent = this.getTerm(termParent);
        // Now create the axiom
        OWLAxiom axiom = df.getOWLSubClassOfAxiom(classChild, classParent);
        // add the axiom to the ontology.
        AddAxiom addAxiom = new AddAxiom(ontology, axiom);
        // We now use the manager to apply the change
        manager.applyChange(addAxiom);
    }

    public List<String> getChildTerm(String termParent) {
        OWLClass classParent = this.getTerm(termParent);
        Set<OWLSubClassOfAxiom> classChildren = ontology.getSubClassAxiomsForSuperClass(classParent);
        List<String> termChildren = new ArrayList<>();

        for (OWLSubClassOfAxiom classChild : classChildren) {
            OWLClass cls = classChild.getSubClass().asOWLClass();
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                termChildren.add(labelFound);
            }
        }

        return termChildren;
    }

    public void getChildTerm(String termParentOriginal, int level, List<String> termChildren) {
        String termParent = termParentOriginal.toLowerCase();
        OWLClass classParent = this.getTerm(termParent);
        Set<OWLSubClassOfAxiom> classChildren = ontology.getSubClassAxiomsForSuperClass(classParent);

        for (OWLSubClassOfAxiom classChild : classChildren) {
            OWLClass cls = classChild.getSubClass().asOWLClass();
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                if (level == 1)
                    termChildren.add(labelFound);
                else
                    getChildTerm(labelFound, level - 1, termChildren);
            }
        }
    }

    private String checkExactRelativeTerm(List<String> listPossibleTerms, String checkingTerm) {
        for (String possibleTerm : listPossibleTerms) {
            if (possibleTerm.equals(checkingTerm))
                return possibleTerm;
        }
        return "";
    }

    private String checkRelativeTerm(List<String> listPossibleTerms, String checkingTerm) {
        for (String possibleTerm : listPossibleTerms) {
            if (possibleTerm.contains(checkingTerm) || checkingTerm.contains(possibleTerm))
                return possibleTerm;
        }
        return "";
    }

    public String getExactParentTerm(String termChildOriginal, String termRoot, int level) {
        List<String> termPossibleParent = new ArrayList<>();
        this.getChildTerm(termRoot, level, termPossibleParent);

        String termChild = termChildOriginal.toLowerCase();
        OWLClass classChild = this.getExactTerm(termChild);
        Set<OWLSubClassOfAxiom> classParents = ontology.getSubClassAxiomsForSubClass(classChild);

        String possibleTerm = checkExactRelativeTerm(termPossibleParent, termChild);
        if (!possibleTerm.equals(""))
            return possibleTerm;

        for (OWLSubClassOfAxiom classParent : classParents) {
            OWLClass cls = classParent.getSuperClass().asOWLClass();
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                String possibleTerm2 = checkExactRelativeTerm(termPossibleParent, labelFound);
                if (!possibleTerm2.equals(""))
                    return possibleTerm2;
                else if (labelFound.equals(termRoot))
                    return "";
                else {
                    return getExactParentTerm(labelFound, termRoot, level);
                }
            }
        }

        return "";
    }

    public String getParentTerm(String termChildOriginal, String termRoot, int level) {
        List<String> termPossibleParent = new ArrayList<>();
        this.getChildTerm(termRoot, level, termPossibleParent);

        String termChild = termChildOriginal.toLowerCase();
        OWLClass classChild = this.getTerm(termChild);
        Set<OWLSubClassOfAxiom> classParents = ontology.getSubClassAxiomsForSubClass(classChild);

        String possibleTerm = checkRelativeTerm(termPossibleParent, termChild);
        if (!possibleTerm.equals(""))
            return possibleTerm;

        for (OWLSubClassOfAxiom classParent : classParents) {
            OWLClass cls = classParent.getSuperClass().asOWLClass();
            String id = cls.getIRI().toString();
            List<String> searchResult = get(cls, RDFS_LABEL.toString());
            if (searchResult.size() > 0) {
                String labelFound = searchResult.get(0);
                String possibleTerm2 = checkRelativeTerm(termPossibleParent, labelFound);
                if (!possibleTerm2.equals(""))
                    return possibleTerm2;
                else if (labelFound.equals(termRoot))
                    return "";
                else {
                    return getParentTerm(labelFound, termRoot, level);
                }
            }
        }

        return "";
    }

    public void printAllChildren(String termRoot) {
        List<String> termChildren = getChildTerm(termRoot);
        if (termChildren.size() > 0) {
            System.out.print(termRoot + "\t");
            System.out.println(termChildren);
            for (String term : termChildren)
                printAllChildren(term);
        }
    }
}