package selfdevelop.transformation.ecommerce;

import selfdevelop.extend.TermDetect;
import selfdevelop.transformation.Ontology;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OntoEcommerce extends Ontology {
    public OntoEcommerce(String defaultDir, String ontologyIRI) {
        super(defaultDir, ontologyIRI);
    }

    private final int ACTIVITY_LEVEL = 2;
    private final int APPLICATION_LEVEL = 3;
    private final int COMPONENT_LEVEL = 1;

    public String detectTask(String entryMessage, TermDetect termDetector) {
        Map<String, String> tokenReceived = termDetector.detectLemmaEnglish2(entryMessage);
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> token : tokenReceived.entrySet()) {
            String tokenType = token.getValue();
            String tokenStr = token.getKey();

            String parentTerm = this.getExactParentTerm(tokenStr, "activity", ACTIVITY_LEVEL);
            if (!parentTerm.equals("")) {
                result.append(parentTerm).append(" ");
            }

            parentTerm = this.getParentTerm(tokenStr, "application", APPLICATION_LEVEL);
            if (!parentTerm.equals("")) {
                result.append(parentTerm).append(" ");
            }

            parentTerm = this.getParentTerm(tokenStr, "component", COMPONENT_LEVEL);
            if (!parentTerm.equals("")) {
                result.append(parentTerm).append(" ");
            }

        }

        if (result.toString().equals(""))
            result.append("unknown");

        return result.toString().trim();
    }
}
