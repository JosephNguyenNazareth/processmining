package selfdevelop.type;

import java.util.HashMap;
import java.util.Map;

public class ActivityBlock {
    private String name;
    private int id;
    private Map<String, AdjacentActivityBlock> in;
    private Map<String, AdjacentActivityBlock> out;

    public ActivityBlock(String name, int id) {
        this.name = name;
        this.id = id;
        in = new HashMap<>();
        out = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, AdjacentActivityBlock> getIn() {
        return in;
    }

    public void setIn(Map<String, AdjacentActivityBlock> in) {
        this.in = in;
    }

    public void addIn(String name, Double execTime) {
        for (AdjacentActivityBlock inAct : this.in.values()) {
            if (inAct.getName().equals(name)) {
                if (!inAct.getExecTimeList().contains(execTime)) {
                    inAct.addExecTime(execTime);
                    break;
                }
            }
        }
    }

    public void addIn(AdjacentActivityBlock newInAct) {
        for (AdjacentActivityBlock inAct : this.in.values()) {
            if (inAct.getName().equals(newInAct.getName())) {
                return;
            }
        }
        this.in.put(newInAct.getName(), newInAct);
    }

    public boolean isIn(String name) {
        for (AdjacentActivityBlock inAct : this.in.values()) {
            if (inAct.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public Map<String, AdjacentActivityBlock> getOut() {
        return out;
    }

    public void setOut(Map<String, AdjacentActivityBlock> out) {
        this.out = out;
    }

    public void addOut(String name, Double execTime) {
        for (AdjacentActivityBlock outAct : this.out.values()) {
            if (outAct.getName().equals(name)) {
                if (!outAct.getExecTimeList().contains(execTime)) {
                    outAct.addExecTime(execTime);
                    break;
                }
            }
        }
    }

    public void addOut(AdjacentActivityBlock newOutAct) {
        for (AdjacentActivityBlock outAct : this.out.values()) {
            if (outAct.getName().equals(newOutAct.getName())) {
                return;
            }
        }
        this.out.put(newOutAct.getName(), newOutAct);
    }

    public Integer getNbInVariant() {
        int sum = 0;
        for (AdjacentActivityBlock adjacentActivityBlock : this.getIn().values()) {
            sum += adjacentActivityBlock.getExecTimeList().size();
        }
        return sum;
    }

    public Integer getNbOutVariant() {
        int sum = 0;
        for (AdjacentActivityBlock adjacentActivityBlock : this.getOut().values()) {
            sum += adjacentActivityBlock.getExecTimeList().size();
        }
        return sum;
    }

    @Override
    public String toString() {
        return "ActivityBlock{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", in=" + in +
                ", out=" + out +
                "}\n";
    }
}
