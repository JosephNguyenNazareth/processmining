package selfdevelop.type;

import java.util.ArrayList;
import java.util.List;

public class AdjacentActivityBlock {
    private String name;
    private String classification;
    private Double prob;
    private List<Double> execTimeList;

    public AdjacentActivityBlock(String name) {
        this.name = name;
        this.classification = "none";
        this.execTimeList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public List<Double> getExecTimeList() {
        return execTimeList;
    }

    public void setExecTimeList(List<Double> execTimeList) {
        this.execTimeList = execTimeList;
    }
    
    public void addExecTime(Double execTime) {
        this.execTimeList.add(execTime);
    }

    public Double getProb() {
        return prob;
    }

    public void setProb(Double prob) {
        this.prob = prob;
    }

    @Override
    public String toString() {
        return "AdjacentActivityBlock{" +
                "name='" + name + '\'' +
                ", classification='" + classification + '\'' +
                ", prob=" + prob +
                ", execTimeList=" + execTimeList +
                '}';
    }
}
