package selfdevelop.type;

public class Artifact {
    String name;
    String transition;

    public Artifact(String name, String transition) {
        this.name = name;
        this.transition = transition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransition() {
        return transition;
    }

    public void setTransition(String transition) {
        this.transition = transition;
    }
}
