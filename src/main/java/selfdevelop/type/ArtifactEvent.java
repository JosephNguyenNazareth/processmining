package selfdevelop.type;

import java.time.LocalDateTime;

public class ArtifactEvent {
    private LocalDateTime timestamp;
    private String artifactId;
    private String artifactType;
    private String action;
    private String status;
    private String activity;
    private String resource;

    public ArtifactEvent(LocalDateTime timestamp, String artifactId, String artifactType, String action, String status, String activity, String resource) {
        this.timestamp = timestamp;
        this.artifactId = artifactId;
        this.artifactType = artifactType;
        this.action = action;
        this.status = status;
        this.activity = activity;
        this.resource = resource;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getArtifactType() {
        return artifactType;
    }

    public void setArtifactType(String artifactType) {
        this.artifactType = artifactType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    @Override
    public String toString() {
        return timestamp.toString() + ','  + artifactId + ',' + artifactType + ',' + action + ','+ status + ',' + activity + ',' + resource;
    }
}
