package selfdevelop.type;

import java.util.ArrayList;

public class ArtifactLog extends ArrayList<ArtifactEvent> {
    public String getTitle() {
        return  "timestamp,artifactId,artifactType,action,status,activity,resource";
    }
}
