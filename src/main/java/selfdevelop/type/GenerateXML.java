package selfdevelop.type;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateXML {
    public String processPath;
    File fXmlFile;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    public Document doc;

    public GenerateXML(String processPath)
    {
        // read the Process Fragment
        this.processPath = processPath;
        fXmlFile = new File(processPath);
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        doc = null;

        try {
            doc = dBuilder.parse(fXmlFile);
        } catch (SAXException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        doc.getDocumentElement().normalize();
    }

    public List<String> analyse(String process, String info, String field) {
        NodeList xmlProcessList =  doc.getElementsByTagName("Process");
        List<String> resultList = new ArrayList<String>();
        for(int i = 0; i < xmlProcessList.getLength(); i++)
        {
            if(xmlProcessList.item(i).getAttributes().getNamedItem("name").getNodeValue().equals(process))
            {
                searchXMLTree(xmlProcessList.item(i).getChildNodes(), info, field, resultList);
                if (resultList.size() > 0)
                    break;
            }
        }
        return resultList;
    }

    private void searchXMLTree(NodeList treeXML, String info, String field, List<String> resultList) {
        for (int j = 0; j < treeXML.getLength(); j++)
        {
            if(treeXML.item(j).getNodeName().equals(info))
            {
                resultList.add(treeXML.item(j).getAttributes().getNamedItem(field).getNodeValue());
            } else {
                if (treeXML.item(j).hasChildNodes())
                    searchXMLTree(treeXML.item(j).getChildNodes(), info, field,  resultList);
            }
        }
    }
}