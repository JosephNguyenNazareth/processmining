package selfdevelop.type;

public class GraphActivity {
    private ActivityBlock activityBlock;
    private String state;
    private int discovered;
    private int finished;
    private String predecessor;

    public GraphActivity(ActivityBlock activityBlock) {
        this.activityBlock = activityBlock;
        this.state = "unexplored";
        this.predecessor = "none";
    }

    public ActivityBlock getActivityBlock() {
        return activityBlock;
    }

    public void setActivityBlock(ActivityBlock activityBlock) {
        this.activityBlock = activityBlock;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(String predecessor) {
        this.predecessor = predecessor;
    }

    public int getDiscovered() {
        return discovered;
    }

    public void setDiscovered(int discovered) {
        this.discovered = discovered;
    }

    public int getFinished() {
        return finished;
    }

    public void setFinished(int finished) {
        this.finished = finished;
    }

    @Override
    public String toString() {
        return "GraphActivity{" +
                "activityBlock=" + activityBlock +
                ", state='" + state + '\'' +
                ", discovered=" + discovered +
                ", finished=" + finished +
                ", predecessor='" + predecessor + '\'' +
                '}';
    }
}
