package selfdevelop.type;

import develop.ExportGraphML;
import develop.HeuristicsGraph;
import develop.MyHeuristicMiner;
import develop.SampleH;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.framework.util.Pair;
import org.processmining.models.heuristics.elements.Activity;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProcessModel {
    private String name;
    private Map<String, ActivityBlock> activityBlockMap;

    public ProcessModel(String name) {
        this.name = name;
        activityBlockMap = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, ActivityBlock> getActivityBlockMap() {
        return activityBlockMap;
    }

    public void discover() {
        SampleH sample = new SampleH();
        String path = "../processmining/example-logs/" + this.name;

        File directoryPath = new File(path);
        String[] files = directoryPath.list();

        List<HeuristicsGraph> listGraph = new ArrayList<>();
        assert files != null;
        for (String file : files) {
            if (file.contains(".xes")) {
                System.out.println(file);
                XLog log = sample.readXESFile(path + "/" + file);
                MyHeuristicMiner newMiner = new MyHeuristicMiner(file, log, 0.90);
                newMiner.mine();
                HeuristicsGraph graph = newMiner.buildGraphic();
                listGraph.add(graph);

                String artifactName = file.substring(0, file.indexOf(".xes"));
//                ExportGraphML miniExporter = new ExportGraphML();
//                miniExporter.exportToGraphML(graph, artifactName, "AppDev");
            }
        }

        ExportGraphML exporter = new ExportGraphML();
        Map<Pair<String, String>, List<String>> mergedForm = new HashMap<>();
        List<Activity> activityList = exporter.getActivities(listGraph, this.name, mergedForm);
        createGraph(activityList, mergedForm);
        manageTimestampActivity(files, path, sample);
    }

    public void manageTimestampActivity(String[] files, String path, SampleH sample) {
        assert files != null;
        for (String file : files) {
            if (file.contains(".xes")) {
                XLog log = sample.readXESFile(path + "/" + file);
                for (XTrace trace : log) {
                    for (int i = 1 ; i < trace.size(); i++){
                        XEvent event = trace.get(i);
                        Map<String, XAttribute> attrs = event.getAttributes();
                        String activityName = attrs.get("concept:name").toString();
                        String timeValue = attrs.get("time:timestamp").toString();

                        List<String> visitedAct = new ArrayList<>();
                        for (int j = i - 1; j >= 0; j--) {
                            XEvent eventBef = trace.get(j);
                            Map<String, XAttribute> attrsBef = eventBef.getAttributes();
                            String activityNameBef = attrsBef.get("concept:name").toString();
                            String timeValueBef = attrsBef.get("time:timestamp").toString();

                            if (activityBlockMap.get(activityName).isIn(activityNameBef)) {
                                if (!visitedAct.contains(activityNameBef)) {
//                                    System.out.println(file);
//                                    System.out.println(activityName + " " + timeValue);
//                                    System.out.println(activityNameBef + " " + timeValueBef);
                                    activityBlockMap.get(activityName).addIn(activityNameBef, diffDateInSeconds(timeValueBef, timeValue));
                                    activityBlockMap.get(activityNameBef).addOut(activityName, diffDateInSeconds(timeValueBef, timeValue));
                                    visitedAct.add(activityNameBef);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public Double diffDateInSeconds(String date1, String date2) {
        SimpleDateFormat obj = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        try {
            Date d1 = obj.parse(date1);
            Date d2 = obj.parse(date2);
            long timeDiff = d2.getTime() - d1.getTime();

            return (double) (timeDiff/1000);
        } catch (ParseException e) {
            SimpleDateFormat obj2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            try {
                Date d1 = obj2.parse(date1);
                Date d2 = obj2.parse(date2);
                long timeDiff = d2.getTime() - d1.getTime();

                return (double) (timeDiff/1000);
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public String startActivity() {
        for (String activityName : this.activityBlockMap.keySet()) {
            if (this.activityBlockMap.get(activityName).getIn().size() == 0
            && this.activityBlockMap.get(activityName).getOut().size() > 0)
                return activityName;
        }

        Random random = new Random();
        int randomStart = random.nextInt(this.activityBlockMap.size() - 1);
        for (String activityName : this.activityBlockMap.keySet()) {
            if (randomStart == 0)
                return activityName;
            randomStart--;
        }

        return "";
    }
    public void createGraph(List<Activity> activityList, Map<Pair<String, String>, List<String>> mergedForm) {
        int idCount = 1;
        for (Activity activity : activityList) {
            String activityName = activity.getLabel().replace("+","");
            ActivityBlock activityBlock = new ActivityBlock(activityName, idCount);
            this.activityBlockMap.put(activityName, activityBlock);
            idCount++;
        }

        for (Map.Entry<Pair<String, String>, List<String>> flow : mergedForm.entrySet()) {
            String source = flow.getKey().getFirst().replace("+","");
            String target = flow.getKey().getSecond().replace("+","");
            activityBlockMap.get(source).addOut(new AdjacentActivityBlock(target));
            activityBlockMap.get(target).addIn(new AdjacentActivityBlock(source));
        }
        
//        setStartActivity(activityBlockMap);
    }

//    public void setStartActivity(Map<String, ActivityBlock> activityBlockMap) {
//        List<String> startingActivities = new ArrayList<>();
//        ActivityBlock oneStartingActivity = null;
//        for (String activityName : activityBlockMap.keySet()) {
//            ActivityBlock activityBlock = activityBlockMap.get(activityName);
//            if (activityBlock.getIn().size() == 0 || (activityBlock.getIn().size() == 1 && activityBlock.getIn().get(0).equals(activityBlock.getName()))) {
//                startingActivities.add(activityName);
//                oneStartingActivity = activityBlock;
//            }
//        }
//
//        if (startingActivities.size() > 1) {
//            this.root = new ActivityBlock("root", 0);
//            this.root.setOut(startingActivities);
//        } else if (startingActivities.size() == 1) {
//            this.root = oneStartingActivity;
//        }
//    }
}
