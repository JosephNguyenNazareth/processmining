package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import java.util.*;

public class ArtifactBapeToXES extends Simulation {
    private String getProjectId(Document issue) {
        return issue.get("description").toString();
    }

    private Map<String, StringBuilder> addContent(MongoCollection<Document> processes) {
        Map<String, StringBuilder> artifactEvent = new HashMap<>();
        String currentProject = getProjectId(Objects.requireNonNull(processes.find().first()));
        List<String> artifactInTrace = new ArrayList<>();
        for (Document process : processes.find()) {
            if (process.get("type").equals("process")) {
                if (!currentProject.equals(getProjectId(process))) {
                    // finish a trace of concerning artifact
                    for (String artifact : artifactInTrace) {
                        artifactEvent.get(artifact).append("\t</trace>\n");
                    }

                    // new trace appears, terminates the list of old trace artifact
                    artifactInTrace.clear();
                }
                currentProject = getProjectId(process);
            }

            if (process.get("artifacts") == null)
                continue;

            List<Document> artifacts = (List<Document>) process.get("artifacts");
            for (Document artifact : artifacts) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.get("name").toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).append("\t<trace>\n");
                    } else {
                        artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                    }
                }
                String eventInfo = "\t\t<event>\n" +
                        "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                        "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                        "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                        "\t\t</event>\n";
                eventInfo = eventInfo.replace("(0)", process.get("actor").toString()).replace("(1)", process.get("timestamp").toString()).replace("(2)", process.get("name").toString());
//              eventInfo = eventInfo.replace("(0)", process.get("actor").toString()).replace("(1)", process.get("timestamp").toString()).replace("(2)", process.get("name").toString() + "_" + artifact.get("usage").toString());
                artifactEvent.get(artifactName).append(eventInfo);
            }
        }
        // finish a trace of concerning artifact
        for (String artifact : artifactInTrace) {
            artifactEvent.get(artifact).append("\t</trace>\n");
        }
        return artifactEvent;
    }

    public void transform() {
        MongoCollection<Document> process = connectMongoDB("bape_gen", "process");

        String processName = "AppDev";

        Map<String, StringBuilder> artifactEvent = addContent(process);

        for (Map.Entry<String, StringBuilder> artifactTraces : artifactEvent.entrySet()) {
            String fileContent = headerXES(processName, "bape" + "_" + artifactTraces.getKey()) + artifactTraces.getValue() + footerXES();
            saveFile(fileContent, processName, "bape" + "_" + artifactTraces.getKey(), "./example-logs/" + processName + "_artifact");
        }
    }
}
