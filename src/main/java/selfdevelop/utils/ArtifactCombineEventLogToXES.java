package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.casestudy.CaseStudy;
import selfdevelop.casestudy.Shopping;
import selfdevelop.extend.AbstractArtifact;
import selfdevelop.extend.TermDetect;
import selfdevelop.simulate.Simulation;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class ArtifactCombineEventLogToXES extends Simulation {

    private String getProjectId(Document issue) {
        String projectKey = issue.get("key").toString();
        return projectKey.substring(0, projectKey.indexOf('-'));
    }

    private String getIssueIid(Document commit) {
        String description = commit.get("description").toString();
        return description.substring(description.indexOf('#') + 1);
    }

    private void instanceMatchingGitlab(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> commits = combination.get("commits");
        MongoCollection<Document> projects = combination.get("gitlab_projects");

        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        List<Document> singleProcess = new ArrayList<>();
        String projectMentioned = projects.find(eq("id", currentProject)).first().get("name").toString();
        projectMentioned = projectMentioned.substring(projectMentioned.indexOf("A"));

        for (Document commit : commits.find()) {
            if (!currentProject.equals(commit.get("project_id").toString())) {
                Map<String, List<Document>> tmp = new HashMap<>();
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                tmp.put("gitlab", tmpProcesses);

                instanceMatch.put(projectMentioned, tmp);
                singleProcess.clear();
            }
            projectMentioned = projects.find(eq("id", commit.get("project_id"))).first().get("name").toString();
            projectMentioned = projectMentioned.substring(projectMentioned.indexOf("A"));

            currentProject = commit.get("project_id").toString();
            singleProcess.add(commit);
        }
        Map<String, List<Document>> tmp = new HashMap<>();
        tmp.put("gitlab", singleProcess);

        instanceMatch.put(projectMentioned, tmp);
    }

    private void instanceMatchingJira(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> issues = combination.get("issues");

        String currentProject = getProjectId(Objects.requireNonNull(issues.find().first()));
        List<Document> singleProcess = new ArrayList<>();

        for (Document issue : issues.find()) {
            if (!currentProject.equals(getProjectId(issue))) {
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                instanceMatch.get(currentProject).put("jira", tmpProcesses);
                singleProcess.clear();
            }
            currentProject = getProjectId(issue);
            singleProcess.add(issue);
        }
        instanceMatch.get(currentProject).put("jira", singleProcess);
    }

    private void instanceMatchingBape(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> bape = combination.get("bape");

        String currentProject = Objects.requireNonNull(bape.find().first()).get("processId").toString();
        List<Document> singleProcess = new ArrayList<>();
        String projectId = Objects.requireNonNull(bape.find(eq("type", "process")).first()).get("description").toString();

        for (Document proc : bape.find()) {
            if (!currentProject.equals(proc.get("processId").toString())) {
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                instanceMatch.get(projectId).put("bape", tmpProcesses);
                singleProcess.clear();
            }
            if (proc.get("type").equals("process"))
                projectId = proc.get("description").toString();
            currentProject = proc.get("processId").toString();
            if (proc.get("type").equals("task"))
                singleProcess.add(proc);
        }
        instanceMatch.get(projectId).put("bape", singleProcess);
    }

    private void instanceSoftwareHeritage(Map<String, MongoCollection<Document>> combination, Map<String, List<Document>> instanceMatch) {
        MongoCollection<Document> commits = combination.get("commits");

        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        List<Document> singleProcess = new ArrayList<>();
        for (Document commit : commits.find()) {
            if (!currentProject.equals(commit.get("project_id").toString())) {
                List<Document> tmpProcess = new ArrayList<>(singleProcess);
                instanceMatch.put(currentProject, tmpProcess);
                singleProcess.clear();
            }

            currentProject = commit.get("project_id").toString();
            singleProcess.add(commit);
        }
        instanceMatch.put(currentProject, singleProcess);
    }

    void buildContentGitlab(MongoCollection<Document> issues, List<Document> commits, MongoCollection<Document> diffs, Map<String, StringBuilder> artifactEvent, List<String> artifactInTrace) {
        for (Document commit : commits) {
            Document issueMentioned = issues.find(eq("iid", Integer.valueOf(getIssueIid(commit)))).first();
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issueMentioned.get("author").toString()).replace("(1)", issueMentioned.get("created_at").toString()).replace("(2)", issueMentioned.get("title").toString());

            Document trulyDiff = Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first());
            List<Document> diffObjs = (List<Document>) trulyDiff.get("diff");
            for (Document artifact : diffObjs) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.get("old_path").toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).append("\t<trace>\n");
                    } else {
                        artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                    }
                }
                artifactEvent.get(artifactName).append(eventInfo);
            }
        }
    }

    void buildContentJira(List<Document> issues, Map<String, StringBuilder> artifactEvent, List<String> artifactInTrace) {
        for (Document issue : issues) {
            if (issue.get("attachment") == null)
                continue;

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issue.get("assignee").toString()).replace("(1)", issue.get("status_changed").toString()).replace("(2)", issue.get("summary").toString());

            List<Object> attachment = (List<Object>) issue.get("attachment");
            for (Object artifact : attachment) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).append("\t<trace>\n");
                    } else {
                        artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                    }
                }
                artifactEvent.get(artifactName).append(eventInfo);
            }
        }
    }

    void buildContentBape(List<Document> procs, String actor,  Map<String, StringBuilder> artifactEvent, List<String> artifactInTrace) {
        for (Document proc : procs) {
            if (proc.get("actor").toString().contains(actor)) {
                if (proc.get("artifacts") == null)
                    continue;

                String eventInfo = "\t\t<event>\n" +
                        "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                        "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                        "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                        "\t\t</event>\n";
                eventInfo = eventInfo.replace("(0)", proc.get("actor").toString()).replace("(1)", proc.get("timestamp").toString()).replace("(2)", proc.get("name").toString());
                List<Document> artifacts = (List<Document>) proc.get("artifacts");
                for (Document artifact : artifacts) {
                    // we expect a simple version where the name of the artifact unchanged
                    String artifactName = artifact.get("name").toString();
                    if (!artifactInTrace.contains(artifactName)) {
                        artifactInTrace.add(artifactName);
                        if (artifactEvent.containsKey(artifactName)) {
                            artifactEvent.get(artifactName).append("\t<trace>\n");
                        } else {
                            artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                        }
                    }
                    artifactEvent.get(artifactName).append(eventInfo);
                }
            }
        }
    }

    void buildContentSoftwareHeritage(List<Document> commits, MongoCollection<Document> diffs, Map<String, List<String>> artifactEvent, List<String> artifactInTrace, CaseStudy realCaseStudy, TermDetect termDetector) {
        for (Document commit : commits) {
            // find the relative task to that commit
            String commitMessage = commit.get("title").toString();
            String expectedTask = realCaseStudy.checkRelevant(commitMessage, termDetector);

//            String eventInfo = "\t\t<event>\n" +
//                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
//                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
//                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
//                    "\t\t</event>\n";
//            eventInfo = eventInfo.replace("(0)", commit.get("committer_name").toString()).replace("(1)", commit.get("created_at").toString()).replace("(2)", expectedTask);

            Document trulyDiff = diffs.find(eq("id", commit.get("id").toString())).first();
            if (trulyDiff == null)
                continue;
//            Document trulyDiff = Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first());
            List<Document> diffObjs = (List<Document>) trulyDiff.get("diff");
            for (Document artifact : diffObjs) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.get("path").toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).add(expectedTask);
                    } else {
                        artifactEvent.put(artifactName, new ArrayList<>());
                    }
                }
                artifactEvent.get(artifactName).add(expectedTask);
            }
        }
    }

    private Map<String, StringBuilder> addContent(Map<String, MongoCollection<Document>> combination) {
        StringBuilder content = new StringBuilder();
        Map<String, Map<String, List<Document>>> instanceMatch = new HashMap<>();
        instanceMatchingGitlab(combination, instanceMatch);
        instanceMatchingJira(combination, instanceMatch);
        instanceMatchingBape(combination, instanceMatch);

        Map<String, StringBuilder> artifactEvent = new HashMap<>();
        List<String> artifactInTrace = new ArrayList<>();

        for (Map.Entry<String, Map<String, List<Document>>> match : instanceMatch.entrySet()) {
            content.append("\t<trace>\n");

            List<Document> issues = match.getValue().get("jira");
            buildContentJira(issues, artifactEvent, artifactInTrace);

            List<Document> procs = match.getValue().get("bape");
            buildContentBape(procs, "Architecture Designer", artifactEvent, artifactInTrace);

            List<Document> commits = match.getValue().get("gitlab");
            buildContentGitlab(combination.get("gitlab_issues"), commits, combination.get("diff"), artifactEvent, artifactInTrace);

            buildContentBape(procs, "Tester", artifactEvent, artifactInTrace);

            for (String artifact: artifactInTrace) {
                artifactEvent.get(artifact).append("\t</trace>\n");
            }

            // new trace appears, terminates the list of old trace artifact
            artifactInTrace.clear();
        }
        return artifactEvent;
    }

    private Map<String, List<String>> addRealContent(Map<String, MongoCollection<Document>> combination, CaseStudy realCaseStudy) {
        StringBuilder content = new StringBuilder();
        Map<String, List<Document>> instanceMatch = new HashMap<>();
        instanceSoftwareHeritage(combination, instanceMatch);

        Map<String, List<String>> artifactEvent = new HashMap<>(); // list of events for each object
        Map<String, List<String>> objectGroups = new HashMap<>();

        TermDetect termDetector = new TermDetect();

        for (Map.Entry<String, List<Document>> match : instanceMatch.entrySet()) {
//            content.append("\t<trace>\n");
            List<Document> commits = match.getValue();
            List<String> artifactInTrace = new ArrayList<>();
            buildContentSoftwareHeritage(commits, combination.get("diff"), artifactEvent, artifactInTrace, realCaseStudy, termDetector);

            // this is where we gonna regroup the objects in the same project
            AbstractArtifact.groupInProject(objectGroups, artifactInTrace, artifactEvent, realCaseStudy, termDetector);

//            for (String artifact: artifactInTrace) {
//                artifactEvent.get(artifact).append("\t</trace>\n");
//            }
        }
        // this is where we gonna regroup the objects from different projects
        Map<String, List<String>> interProjectGroup = new HashMap<>();
        AbstractArtifact.groupInterProjects(objectGroups, interProjectGroup);

        return artifactEvent;
    }

    public void transform(String processName, String directory) {
        Map<String, MongoCollection<Document>> eventLogCombination = new HashMap<>();
        eventLogCombination.put("issues", connectMongoDB("jira_gen", "issues"));

        eventLogCombination.put("gitlab_projects", connectMongoDB("gitlab_gen", "projects"));
        eventLogCombination.put("gitlab_issues", connectMongoDB("gitlab_gen", "issues"));
        eventLogCombination.put("commits", connectMongoDB("gitlab_gen", "commits"));
        eventLogCombination.put("diff", connectMongoDB("gitlab_gen", "diffs"));
        eventLogCombination.put("bape", connectMongoDB("bape_gen", "process"));

        Map<String, StringBuilder> artifactEvent = addContent(eventLogCombination);

        for (Map.Entry<String, StringBuilder> artifactTraces: artifactEvent.entrySet()) {
            String fileContent = headerXES(processName,  artifactTraces.getKey()) + artifactTraces.getValue() + footerXES();
            saveFile(fileContent, processName, artifactTraces.getKey(),directory + "example-logs/" + processName + "_artifact");
        }
    }

    public void transformReal(String processName, String directory) {
        Map<String, MongoCollection<Document>> eventLogCombination = new HashMap<>();
        String dbName = "software_heritage";
        eventLogCombination.put("projects", connectMongoDB(dbName, "projects"));
        eventLogCombination.put("commits", connectMongoDB(dbName, "commits"));
        eventLogCombination.put("diff", connectMongoDB(dbName, "diffs"));

        CaseStudy shoppingCaseStudy = new Shopping(processName);
        shoppingCaseStudy.createConcept();
        Map<String, List<String>> artifactEvent = addRealContent(eventLogCombination, shoppingCaseStudy);

//        for (Map.Entry<String, StringBuilder> artifactTraces: artifactEvent.entrySet()) {
//            String fileContent = headerXES(processName,  artifactTraces.getKey()) + artifactTraces.getValue() + footerXES();
//            saveFile(fileContent, processName, artifactTraces.getKey(),directory + "example-logs/" + processName + "_artifact");
//        }
    }
}
