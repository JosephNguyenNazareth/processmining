package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class ArtifactGitlabToXES extends Simulation {
    private String getIssueIid(Document commit) {
        String description = commit.get("description").toString();
        return description.substring(description.indexOf('#') + 1);
    }

    private Map<String, StringBuilder> addContent (MongoCollection<Document> issues, MongoCollection<Document> commits, MongoCollection<Document> diffs) {
        Map<String, StringBuilder> artifactEvent = new HashMap<>();
        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        List<String> artifactInTrace = new ArrayList<>();
        for (Document commit : commits.find()) {
            Document issueMentioned = issues.find(eq("iid", Integer.valueOf(getIssueIid(commit)))).first();
            if (!currentProject.equals(commit.get("project_id").toString())) {
                // finish a trace of concerning artifact
                for (String artifact: artifactInTrace) {
                    artifactEvent.get(artifact).append("\t</trace>\n");
                }

                // new trace appears, terminates the list of old trace artifact
                artifactInTrace.clear();
            }
            currentProject = commit.get("project_id").toString();

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issueMentioned.get("author").toString()).replace("(1)", issueMentioned.get("created_at").toString()).replace("(2)", issueMentioned.get("title").toString());

            Document trulyDiff = Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first());
            List<Document> diffObjs = (List<Document>) trulyDiff.get("diff");
            for (Document artifact: diffObjs) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.get("old_path").toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).append("\t<trace>\n");
                    } else {
                        artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                    }
                }
                artifactEvent.get(artifactName).append(eventInfo);
            }
        }
        // finish a trace of concerning artifact
        for (String artifact: artifactInTrace) {
            artifactEvent.get(artifact).append("\t</trace>\n");
        }
        return artifactEvent;
    }

    public void transform() {
        MongoCollection<Document> issues = connectMongoDB("gitlab_gen","issues");
        MongoCollection<Document> commits = connectMongoDB("gitlab_gen","commits");
        MongoCollection<Document> diff = connectMongoDB("gitlab_gen","diffs");

        String processName = "AppDev";

        Map<String, StringBuilder> artifactEvent = addContent(issues, commits, diff);

        for (Map.Entry<String, StringBuilder> artifactTraces: artifactEvent.entrySet()) {
            String fileContent = headerXES(processName, "gitlab" + "_" + artifactTraces.getKey()) + artifactTraces.getValue() + footerXES();
            saveFile(fileContent, processName, "gitlab2" + "_" + artifactTraces.getKey(),"./example-logs/" + processName + "_artifact");
        }
    }
}
