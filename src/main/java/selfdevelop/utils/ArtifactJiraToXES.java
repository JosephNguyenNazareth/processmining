package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class ArtifactJiraToXES extends Simulation {
    private String getProjectId(Document issue) {
        String projectKey = issue.get("key").toString();
        return projectKey.substring(0, projectKey.indexOf('-'));
    }

    private Map<String, StringBuilder> addContent (MongoCollection<Document> issues) {
        Map<String, StringBuilder> artifactEvent = new HashMap<>();
        String currentProject = getProjectId(Objects.requireNonNull(issues.find().first()));
        List<String> artifactInTrace = new ArrayList<>();
        for (Document issue : issues.find()) {
            if (issue.get("status").toString().equals("To do"))
                continue;

            if (!currentProject.equals(getProjectId(issue))) {
                // finish a trace of concerning artifact
                for (String artifact: artifactInTrace) {
                    artifactEvent.get(artifact).append("\t</trace>\n");
                }

                // new trace appears, terminates the list of old trace artifact
                artifactInTrace.clear();
            }
            currentProject = getProjectId(issue);

            if (issue.get("attachment") == null)
                continue;

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issue.get("assignee").toString()).replace("(1)", issue.get("status_changed").toString()).replace("(2)", issue.get("summary").toString());

            List<Object> attachment = (List<Object>) issue.get("attachment");
            for (Object artifact: attachment) {
                // we expect a simple version where the name of the artifact unchanged
                String artifactName = artifact.toString();
                if (!artifactInTrace.contains(artifactName)) {
                    artifactInTrace.add(artifactName);
                    if (artifactEvent.containsKey(artifactName)) {
                        artifactEvent.get(artifactName).append("\t<trace>\n");
                    } else {
                        artifactEvent.put(artifactName, new StringBuilder("\t<trace>\n"));
                    }
                }
                artifactEvent.get(artifactName).append(eventInfo);
            }
        }
        // finish a trace of concerning artifact
        for (String artifact: artifactInTrace) {
            artifactEvent.get(artifact).append("\t</trace>\n");
        }
        return artifactEvent;
    }

    public void transform() {
        MongoCollection<Document> issues = connectMongoDB("jira_gen","issues");

        String processName = "AppDev";

        Map<String, StringBuilder> artifactEvent = addContent(issues);

        for (Map.Entry<String, StringBuilder> artifactTraces: artifactEvent.entrySet()) {
            String fileContent = headerXES(processName, "jira" + "_" + artifactTraces.getKey()) + artifactTraces.getValue() + footerXES();
            saveFile(fileContent, processName, "jira" + "_" + artifactTraces.getKey(),"./example-logs/" + processName + "_artifact");
        }
    }
}
