package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class CombineEventLogToXES extends Simulation {
    private String getProjectId(Document issue) {
        String projectKey = issue.get("key").toString();
        return projectKey.substring(0, projectKey.indexOf('-'));
    }

    private String getIssueIid(Document commit) {
        String description = commit.get("description").toString();
        return description.substring(description.indexOf('#') + 1);
    }

    private void instanceMatchingGitlab(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> commits = combination.get("commits");
        MongoCollection<Document> projects = combination.get("gitlab_projects");

        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        List<Document> singleProcess = new ArrayList<>();
        String projectMentioned = projects.find(eq("id", currentProject)).first().get("name").toString();
        projectMentioned = projectMentioned.substring(projectMentioned.indexOf("A"));

        for (Document commit : commits.find()) {
            if (!currentProject.equals(commit.get("project_id").toString())) {
                Map<String, List<Document>> tmp = new HashMap<>();
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                tmp.put("gitlab", tmpProcesses);

                instanceMatch.put(projectMentioned, tmp);
                singleProcess.clear();
            }
            projectMentioned = projects.find(eq("id", commit.get("project_id"))).first().get("name").toString();
            projectMentioned = projectMentioned.substring(projectMentioned.indexOf("A"));

            currentProject = commit.get("project_id").toString();
            singleProcess.add(commit);
        }
        Map<String, List<Document>> tmp = new HashMap<>();
        tmp.put("gitlab", singleProcess);

        instanceMatch.put(projectMentioned, tmp);
    }

    private void instanceMatchingJira(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> issues = combination.get("issues");

        String currentProject = getProjectId(Objects.requireNonNull(issues.find().first()));
        List<Document> singleProcess = new ArrayList<>();

        for (Document issue : issues.find()) {
            if (!currentProject.equals(getProjectId(issue))) {
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                instanceMatch.get(currentProject).put("jira", tmpProcesses);
                singleProcess.clear();
            }
            currentProject = getProjectId(issue);
            singleProcess.add(issue);
        }
        instanceMatch.get(currentProject).put("jira", singleProcess);
    }

    private void instanceMatchingBape(Map<String, MongoCollection<Document>> combination, Map<String, Map<String, List<Document>>> instanceMatch) {
        MongoCollection<Document> bape = combination.get("bape");

        String currentProject = Objects.requireNonNull(bape.find().first()).get("processId").toString();
        List<Document> singleProcess = new ArrayList<>();
        String projectId = Objects.requireNonNull(bape.find(eq("type", "process")).first()).get("description").toString();

        for (Document proc : bape.find()) {
            if (!currentProject.equals(proc.get("processId").toString())) {
                List<Document> tmpProcesses = new ArrayList<>(singleProcess);
                instanceMatch.get(projectId).put("bape", tmpProcesses);
                singleProcess.clear();
            }
            if (proc.get("type").equals("process"))
                projectId = proc.get("description").toString();
            currentProject = proc.get("processId").toString();
            if (proc.get("type").equals("task"))
                singleProcess.add(proc);
        }
        instanceMatch.get(projectId).put("bape", singleProcess);
    }

    void buildContentGitlab(List<Document> commits, MongoCollection<Document> diffs, StringBuilder content) {
        for (Document commit : commits) {
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", commit.get("committer_name").toString()).replace("(1)", commit.get("created_at").toString()).replace("(2)", commit.get("title").toString());
            eventInfo = eventInfo.replace("(3)", addContentArtifactGitlab(Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first())));
            content.append(eventInfo);
        }
    }

    void buildContentGitlab(MongoCollection<Document> issues, List<Document> commits, MongoCollection<Document> diffs, StringBuilder content) {
        for (Document commit : commits) {
            Document issueMentioned = issues.find(eq("iid", Integer.valueOf(getIssueIid(commit)))).first();
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issueMentioned.get("author").toString()).replace("(1)", issueMentioned.get("created_at").toString()).replace("(2)", issueMentioned.get("title").toString());
            eventInfo = eventInfo.replace("(3)", addContentArtifactGitlab(Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first())));
            content.append(eventInfo);
        }
    }

    void buildContentJira(List<Document> issues, StringBuilder content) {
        for (Document issue : issues) {
//            if (issue.get("status").toString().equals("To do"))
//                continue;

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issue.get("assignee").toString()).replace("(1)", issue.get("status_changed").toString()).replace("(2)", issue.get("summary").toString());

            if (issue.get("attachment") != null)
                eventInfo = eventInfo.replace("(3)", addContentArtifactJira(Objects.requireNonNull(issue)));
            else
                eventInfo = eventInfo.replace("(3)", "");

            content.append(eventInfo);
        }
    }

    void buildContentBape(List<Document> procs, StringBuilder content, String actor) {
        for (Document proc : procs) {
            if (proc.get("actor").toString().contains(actor)) {
                String eventInfo = "\t\t<event>\n" +
                        "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                        "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                        "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                        "(3)" +
                        "\t\t</event>\n";
                eventInfo = eventInfo.replace("(0)", proc.get("actor").toString()).replace("(1)", proc.get("timestamp").toString()).replace("(2)", proc.get("name").toString());

                if (proc.get("artifacts") != null)
                    eventInfo = eventInfo.replace("(3)", addContentArtifactBape(Objects.requireNonNull(proc)));
                else
                    eventInfo = eventInfo.replace("(3)", "");

                content.append(eventInfo);
            }
        }
    }

    private void addContent(StringBuilder fileContent, Map<String, MongoCollection<Document>> combination) {
        StringBuilder content = new StringBuilder();
        Map<String, Map<String, List<Document>>> instanceMatch = new HashMap<>();
        instanceMatchingGitlab(combination, instanceMatch);
        instanceMatchingJira(combination, instanceMatch);
        instanceMatchingBape(combination, instanceMatch);

        for (Map.Entry<String, Map<String, List<Document>>> match : instanceMatch.entrySet()) {
            content.append("\t<trace>\n");

            List<Document> issues = match.getValue().get("jira");
            buildContentJira(issues, content);

            List<Document> procs = match.getValue().get("bape");
            buildContentBape(procs, content, "Architecture Designer");

            List<Document> commits = match.getValue().get("gitlab");
            buildContentGitlab(combination.get("gitlab_issues"), commits, combination.get("diff"), content);

            buildContentBape(procs, content, "Tester");

            content.append("\t</trace>\n");
        }
        fileContent.append(content);
    }

    private String addContentArtifactGitlab(Document diff) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";

        StringBuilder artifactEntries = new StringBuilder();
        List<Document> diff_objs = (List<Document>) diff.get("diff");
        for (Document artifact: diff_objs) {
            String transition = "";
            if (artifact.get("new_file").toString().equals("true"))
                transition = "created";
            else if (artifact.get("renamed_file").toString().equals("true"))
                transition = "renamed";
            else if (artifact.get("deleted_file").toString().equals("true"))
                transition = "deleted";
            else if (!artifact.get("old_path").toString().equals(artifact.get("new_path").toString()))
                transition = "moved";
            else
                transition = "modified";
            String tmp = artifactMove.replace("(0)", artifact.get("old_path").toString()).replace("(1)", artifact.get("old_path").toString()).replace("(2)", transition);
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());

        return artifactDescription;
    }

    private String addContentArtifactJira(Document issue) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";

        StringBuilder artifactEntries = new StringBuilder();
        List<Object> attachment = (List<Object>) issue.get("attachment");
        for (Object artifact: attachment) {
            String transition = "created";
            String tmp = artifactMove.replace("(0)", artifact.toString()).replace("(1)", artifact.toString()).replace("(2)", transition);
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());

        return artifactDescription;
    }

    private String addContentArtifactBape(Document proc) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";

        StringBuilder artifactEntries = new StringBuilder();
        List<Document> artifacts = (List<Document>) proc.get("artifacts");
        for (Document artifact: artifacts) {
            String tmp = artifactMove.replace("(0)", artifact.get("name").toString()).replace("(1)", artifact.get("name").toString()).replace("(2)", artifact.get("usage").toString());
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());

        return artifactDescription;
    }

    public void transform() {
        Map<String, MongoCollection<Document>> eventLogCombination = new HashMap<>();
        eventLogCombination.put("issues", connectMongoDB("jira_gen","issues"));

        eventLogCombination.put("gitlab_projects", connectMongoDB("gitlab_gen","projects"));
        eventLogCombination.put("gitlab_issues", connectMongoDB("gitlab_gen","issues"));
        eventLogCombination.put("commits", connectMongoDB("gitlab_gen","commits"));
        eventLogCombination.put("diff", connectMongoDB("gitlab_gen","diffs"));
        eventLogCombination.put("bape", connectMongoDB("bape_gen","process"));


        String processName = "AppDev";
        StringBuilder fileContent = new StringBuilder(headerXES(processName));

        addContent(fileContent, eventLogCombination);

        fileContent.append(footerXES());
        saveFile(fileContent.toString(), processName, "combine", "./example-logs/" + processName);
    }
}
