package selfdevelop.utils;

import selfdevelop.type.ActivityBlock;
import selfdevelop.type.GraphActivity;
import selfdevelop.type.AdjacentActivityBlock;
import selfdevelop.type.ProcessModel;

import java.util.HashMap;
import java.util.Map;

public class DFS {
    private Map<String, GraphActivity> activityMap;
    private int time;

    public DFS(ProcessModel processModel) {
        this.activityMap = new HashMap<>();
        this.time = 0;
        for (String activityName : processModel.getActivityBlockMap().keySet()) {
            ActivityBlock activityBlock = processModel.getActivityBlockMap().get(activityName);
            activityMap.put(activityName, new GraphActivity(activityBlock));
        }
    }

    public Map<String, GraphActivity> getActivityMap() {
        return activityMap;
    }

    public void setActivityMap(Map<String, GraphActivity> activityMap) {
        this.activityMap = activityMap;
    }

    public void search() {
        for (String activityName : this.activityMap.keySet()) {
            if (this.activityMap.get(activityName).getState().equals("unexplored"))
                dfsVisit(this.activityMap.get(activityName));
        }
    }

    public void dfsVisit(GraphActivity graphActivity) {
        graphActivity.setState("discovered");
        this.time++;
        graphActivity.setDiscovered(this.time);

        for (AdjacentActivityBlock adjacentActivityBlock : graphActivity.getActivityBlock().getIn().values()) {
            GraphActivity otherGraphActivity = this.activityMap.get(adjacentActivityBlock.getName());
            if (otherGraphActivity.getState().equals("unexplored")) {
                otherGraphActivity.setPredecessor(graphActivity.getActivityBlock().getName());
                graphActivity.getActivityBlock().getIn().get(otherGraphActivity.getActivityBlock().getName()).setClassification("tree_edge");
                dfsVisit(otherGraphActivity);
            } else if (otherGraphActivity.getState().equals("discovered")) {
                graphActivity.getActivityBlock().getIn().get(otherGraphActivity.getActivityBlock().getName()).setClassification("back_edge");
            } else if (graphActivity.getDiscovered() > otherGraphActivity.getDiscovered())
                graphActivity.getActivityBlock().getIn().get(otherGraphActivity.getActivityBlock().getName()).setClassification("cross_edge");
            else
                graphActivity.getActivityBlock().getIn().get(otherGraphActivity.getActivityBlock().getName()).setClassification("forward_edge");
        }
        graphActivity.setState("finished");
        this.time++;
        graphActivity.setFinished(this.time);
    }

    @Override
    public String toString() {
        return "DFS{" +
                "activityMap=" + activityMap +
                '}';
    }
}
