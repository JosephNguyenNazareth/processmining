package selfdevelop.utils;

import selfdevelop.analysis.VariantAnalysis;
import selfdevelop.type.ActivityBlock;
import selfdevelop.type.GraphActivity;
import selfdevelop.type.ProcessModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class DrawIOExport {
    private ProcessModel processModel;
    private String header;
    private String footer;
    private String block;
    private String arc;

    public DrawIOExport (String processName) {
        this.processModel = new ProcessModel(processName);
        processModel.discover();
        this.init();
    }

    private void init() {
        this.header = "<mxfile host=\"app.diagrams.net\" modified=\"2023-06-26T09:43:36.948Z\" agent=\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36\" version=\"21.5.0\" pages=\"1\">\n" +
                "  <diagram id=\"1n_F53SqJsbJG-LN5a4a\" name=\"Page-1\">\n" +
                "    <mxGraphModel dx=\"1399\" dy=\"709\" grid=\"1\" gridSize=\"10\" guides=\"1\" tooltips=\"1\" connect=\"1\" arrows=\"1\" fold=\"1\" page=\"1\" pageScale=\"1\" pageWidth=\"1169\" pageHeight=\"827\" math=\"0\" shadow=\"0\">\n" +
                "      <root>\n" +
                "        <mxCell id=\"0\" />\n" +
                "        <mxCell id=\"1\" parent=\"0\" />\n";
        this.footer = "      </root>\n" +
                "    </mxGraphModel>\n" +
                "  </diagram>\n" +
                "</mxfile>";
        this.block = "        <mxCell id=\"activity-(0)\" value=\"(1)\" style=\"rounded=0;whiteSpace=wrap;html=1;\" vertex=\"1\" parent=\"1\">\n" +
                "          <mxGeometry x=\"(2)\" y=\"(3)\" width=\"120\" height=\"60\" as=\"geometry\" />\n" +
                "        </mxCell>\n";
        this.arc = "        <mxCell id=\"activity-(0)\" value=\"(1)\" style=\"rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0;entryY=0.5;entryDx=0;entryDy=0;\" edge=\"1\" parent=\"1\" source=\"activity-(2)\" target=\"activity-(3)\">\n" +
                "          <mxGeometry relative=\"1\" as=\"geometry\" />\n" +
                "        </mxCell>\n";
    }

    public void exportProcessModel() {
//        VariantAnalysis variantAnalysis =  new VariantAnalysis(this.processModel);
//        variantAnalysis.analyze();

        Map<String, Integer> idBlockMap = new HashMap<>();
        Map<String, Integer> idArcMap = new HashMap<>();

        int counter = 1;
        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            idBlockMap.put(activityName,  counter);
            counter++;
        }

        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            for (String adjacentActivity : this.processModel.getActivityBlockMap().get(activityName).getOut().keySet()) {
                String sourceId = idBlockMap.get(activityName).toString();
                String targetId = idBlockMap.get(adjacentActivity).toString();
                idArcMap.put(sourceId + "-" + targetId, this.processModel.getActivityBlockMap().get(activityName).getOut().get(adjacentActivity).getExecTimeList().size());
            }
        }

        StringBuilder content = new StringBuilder(this.header);
        for (String arc : idArcMap.keySet()) {
            String arcContent = this.arc;
            arcContent = arcContent.replace("(0)", String.valueOf(counter)).replace("(1)", idArcMap.get(arc).toString());
            arcContent = arcContent.replace("(2)", arc.substring(0, arc.indexOf("-"))).replace("(3)", arc.substring(arc.indexOf("-") + 1));
            content.append(arcContent);
            counter++;
        }

        bfs(content, idBlockMap);

        content.append(this.footer);
        saveFile(content.toString(), this.processModel.getName(), "../processmining/example-logs/" + this.processModel.getName());
    }

    public void exportProcessVariant() {
        VariantAnalysis variantAnalysis =  new VariantAnalysis(this.processModel);
        variantAnalysis.analyze();

        Map<String, Integer> idBlockMap = new HashMap<>();
        Map<String, Double> idArcMap = new HashMap<>();

        int counter = 1;
        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            idBlockMap.put(activityName,  counter);
            counter++;
        }

        for (String activityName : this.processModel.getActivityBlockMap().keySet()) {
            for (String adjacentActivity : this.processModel.getActivityBlockMap().get(activityName).getOut().keySet()) {
                String sourceId = idBlockMap.get(activityName).toString();
                String targetId = idBlockMap.get(adjacentActivity).toString();
                idArcMap.put(sourceId + "-" + targetId,  this.processModel.getActivityBlockMap().get(activityName).getOut().get(adjacentActivity).getProb());
            }
        }

        StringBuilder content = new StringBuilder(this.header);
        for (String arc : idArcMap.keySet()) {
            String arcContent = this.arc;
            DecimalFormat decimalFormat = new DecimalFormat("#.##");
            arcContent = arcContent.replace("(0)", String.valueOf(counter)).replace("(1)", decimalFormat.format(idArcMap.get(arc)));
            arcContent = arcContent.replace("(2)", arc.substring(0, arc.indexOf("-"))).replace("(3)", arc.substring(arc.indexOf("-") + 1));
            content.append(arcContent);
            counter++;
        }

        bfs(content, idBlockMap);

        content.append(this.footer);
        saveFile(content.toString(), this.processModel.getName() + "_variant", "../processmining/example-logs/" + this.processModel.getName());
    }

    private void bfs(StringBuilder content, Map<String, Integer> idBlockMap) {
        Map<String, GraphActivity> activityMap = new HashMap<>();
        int x = 120;
        int y = 80;
        int width = 120;
        int height = 60;

        for (String activityName : processModel.getActivityBlockMap().keySet()) {
            ActivityBlock activityBlock = processModel.getActivityBlockMap().get(activityName);
            activityMap.put(activityName, new GraphActivity(activityBlock));
        }

        List<String> activityQueue = new ArrayList<>();
        String startActivityName = this.processModel.startActivity();
        if (startActivityName.equals(""))
            return;

        activityMap.get(startActivityName).setState("discovered");
        String startActivityContent = this.block;
        startActivityContent = startActivityContent.replace("(0)",idBlockMap.get(startActivityName).toString()).replace("(1)",startActivityName);
        startActivityContent = startActivityContent.replace("(2)", String.valueOf(x)).replace("(3)", String.valueOf(y));
        content.append(startActivityContent);
        activityQueue.add(startActivityName);

        while (activityQueue.size() > 0) {
            String chosenActivityName = activityQueue.remove(0);
            x += 2 * width;
            y = 80;
            for (String adjacentActivityName : activityMap.get(chosenActivityName).getActivityBlock().getOut().keySet()) {
                if (activityMap.get(adjacentActivityName).getState().equals("unexplored")) {
                    activityMap.get(adjacentActivityName).setState("discovered");

                    String activityContent = this.block;
                    activityContent = activityContent.replace("(0)",idBlockMap.get(adjacentActivityName).toString()).replace("(1)",adjacentActivityName);
                    activityContent = activityContent.replace("(2)", String.valueOf(x)).replace("(3)", String.valueOf(y));
                    y += 2 * height;
                    content.append(activityContent);

                    activityMap.get(adjacentActivityName).setPredecessor(chosenActivityName);
                    activityQueue.add(adjacentActivityName);
                }
            }
            activityMap.get(chosenActivityName).setState("finished");
        }
    }

    private void saveFile(String content, String processName, String path) {
        try {
            File savePath = new File(path);
            if (!savePath.isDirectory()) {
                savePath.mkdir();
            }

            FileWriter myWriter = new FileWriter(path + "/" + processName + ".drawio");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Written successfully");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
