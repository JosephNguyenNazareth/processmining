package selfdevelop.utils;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import static com.mongodb.client.model.Filters.eq;
import java.util.List;
import java.util.Objects;

public class GitlabToXES extends Simulation {
    private String getIssueIid(Document commit) {
        String description = commit.get("description").toString();
        return description.substring(description.indexOf('#') + 1);
    }

    private void addContent(StringBuilder fileContent, MongoCollection<Document> issues, MongoCollection<Document> commits, MongoCollection<Document> diffs) {
        StringBuilder content = new StringBuilder("\t<trace>\n");
        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        for (Document commit : commits.find()) {
            Document issueMentioned = issues.find(eq("iid", Integer.valueOf(getIssueIid(commit)))).first();
            if (!currentProject.equals(commit.get("project_id").toString()))
                content.append("\t</trace>\n").append("\t<trace>\n");
            currentProject = commit.get("project_id").toString();

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issueMentioned.get("author").toString()).replace("(1)", issueMentioned.get("created_at").toString()).replace("(2)", issueMentioned.get("title").toString());
            eventInfo = eventInfo.replace("(3)", addContentArtifact(Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first())));
            content.append(eventInfo);
        }
        content.append("\t</trace>\n");
        fileContent.append(content);
    }

    private void addContentCommit(StringBuilder fileContent, MongoCollection<Document> commits, MongoCollection<Document> diffs) {
        StringBuilder content = new StringBuilder("\t<trace>\n");
        String currentProject = Objects.requireNonNull(commits.find().first()).get("project_id").toString();
        for (Document commit : commits.find()) {
            if (!currentProject.equals(commit.get("project_id").toString()))
                content.append("\t</trace>\n").append("\t<trace>\n");
            currentProject = commit.get("project_id").toString();
            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", commit.get("committer_name").toString()).replace("(1)", commit.get("created_at").toString()).replace("(2)", commit.get("title").toString());
            eventInfo = eventInfo.replace("(3)", addContentArtifact(Objects.requireNonNull(diffs.find(eq("id", commit.get("id").toString())).first())));
            content.append(eventInfo);
        }
        content.append("\t</trace>\n");
        fileContent.append(content);
    }

    private String addContentArtifact(Document diff) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";

        StringBuilder artifactEntries = new StringBuilder();
        List<Document> diff_objs = (List<Document>) diff.get("diff");
        for (Document artifact: diff_objs) {
            String transition = "";
            if (artifact.get("new_file").toString().equals("true"))
                transition = "created";
            else if (artifact.get("renamed_file").toString().equals("true"))
                transition = "renamed";
            else if (artifact.get("deleted_file").toString().equals("true"))
                transition = "deleted";
            else if (!artifact.get("old_path").toString().equals(artifact.get("new_path").toString()))
                transition = "moved";
            else
                transition = "modified";
            String tmp = artifactMove.replace("(0)", artifact.get("old_path").toString()).replace("(1)", artifact.get("old_path").toString()).replace("(2)", transition);
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());

        return artifactDescription;
    }

    public void transform() {
        MongoCollection<Document> issues = connectMongoDB("gitlab_gen","issues");
        MongoCollection<Document> commits = connectMongoDB("gitlab_gen","commits");
        MongoCollection<Document> diff = connectMongoDB("gitlab_gen","diffs");

        String processName = "AppDev";
        StringBuilder fileContent = new StringBuilder(headerXES(processName, "gitlab"));

        addContent(fileContent, issues, commits, diff);
//        addContentCommit(fileContent, commits, diff);

        fileContent.append(footerXES());
        saveFile(fileContent.toString(), processName, "gitlab2","./example-logs/" + processName);
    }
}
