package selfdevelop.utils;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import selfdevelop.simulate.Simulation;

import java.util.List;
import java.util.Objects;

import static com.mongodb.client.model.Filters.eq;

public class JiraToXES extends Simulation {
    private String getProjectId(Document issue) {
        String projectKey = issue.get("key").toString();
        return projectKey.substring(0, projectKey.indexOf('-'));
    }

    private void addContent(StringBuilder fileContent, MongoCollection<Document> issues) {
        StringBuilder content = new StringBuilder("\t<trace>\n");
        String currentProject = getProjectId(Objects.requireNonNull(issues.find().first()));
        for (Document issue : issues.find()) {
            if (issue.get("status").toString().equals("To do"))
                continue;

            if (!currentProject.equals(getProjectId(issue)))
                content.append("\t</trace>\n").append("\t<trace>\n");
            currentProject = getProjectId(issue);

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "(3)" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", issue.get("assignee").toString()).replace("(1)", issue.get("status_changed").toString()).replace("(2)", issue.get("summary").toString());

            if (issue.get("attachment") != null)
                eventInfo = eventInfo.replace("(3)", addContentArtifact(Objects.requireNonNull(issue)));
            else
                eventInfo = eventInfo.replace("(3)", "");

            content.append(eventInfo);
        }
        content.append("\t</trace>\n");
        fileContent.append(content);
    }

    private String addContentArtifact(Document issue) {
        String artifactDescription = "\t\t\t<list key=\"artifactlifecycle:moves\">\n" +
                "\t\t\t<values>\n" +
                "(*)" +
                "\t\t\t</values>\n" +
                "\t\t\t</list>\n";
        String artifactMove = "\t\t\t\t<string key=\"artifactlifecycle:model\" value=\"(0)\">\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:instance\" value=\"(1)\"/>\n" +
                "\t\t\t\t\t<string key=\"artifactlifecycle:transition\" value=\"(2)\"/>\n" +
                "\t\t\t\t</string>\n";

        StringBuilder artifactEntries = new StringBuilder();
        List<Object> attachment = (List<Object>) issue.get("attachment");
        for (Object artifact: attachment) {
            String transition = "created";
            String tmp = artifactMove.replace("(0)", artifact.toString()).replace("(1)", artifact.toString()).replace("(2)", transition);
            artifactEntries.append(tmp);
        }
        artifactDescription = artifactDescription.replace("(*)", artifactEntries.toString());

        return artifactDescription;
    }

    public void transform() {
        MongoCollection<Document> issues = connectMongoDB("jira_gen","issues");

        String processName = "AppDev";
        StringBuilder fileContent = new StringBuilder(headerXES(processName, "jira"));

        addContent(fileContent, issues);

        fileContent.append(footerXES());
        saveFile(fileContent.toString(), processName, "jira", "./example-logs/" + processName);
    }
}
