package selfdevelop.utils;

import java.util.*;

public class Levenshtein {
    public static int computeDistance(String str1, String str2)
    {
        int[][] dp = new int[str1.length() + 1][str2.length() + 1];

        for (int i = 0; i <= str1.length(); i++)
        {
            for (int j = 0; j <= str2.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }

                else {
                    dp[i][j] = minEdits(dp[i - 1][j - 1]
                                    + numberReplacement(str1.charAt(i - 1),str2.charAt(j - 1)), // replace
                            dp[i - 1][j] + 1, // delete
                            dp[i][j - 1] + 1); // insert
                }
            }
        }

        return dp[str1.length()][str2.length()];
    }


    static int numberReplacement(char c1, char c2)
    {
        return c1 == c2 ? 0 : 1;
    }

    public static int computeDistanceStringList(List<String> stringList1, List<String> stringList2)
    {
        int[][] dp = new int[stringList1.size() + 1][stringList2.size() + 1];

        for (int i = 0; i <= stringList1.size(); i++)
        {
            for (int j = 0; j <= stringList2.size(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }

                else {
                    dp[i][j] = minEdits(dp[i - 1][j - 1]
                                    + numberReplacementStringList(stringList1.get(i - 1),stringList2.get(j - 1)), // replace
                            dp[i - 1][j] + 1, // delete
                            dp[i][j - 1] + 1); // insert
                }
            }
        }

        return dp[stringList1.size()][stringList2.size()];
    }


    static int numberReplacementStringList(String str1, String str2)
    {
        return str1.equals(str2) ? 0 : 1;
    }

    static int minEdits(int... nums)
    {

        return Arrays.stream(nums).min().orElse(
                Integer.MAX_VALUE);
    }

    static String similarString(String str1, String str2) {
        int farthestSimilar = 0;
        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                farthestSimilar = i - 1;
            }
        }
        return str1.substring(0, farthestSimilar);
    }

    static String[] diffStrings(String str1, String str2) {
        int farthestSimilar = 0;
        String[] differentSubstrings = new String[2];
        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                farthestSimilar = i - 1;
            }
        }
        differentSubstrings[0] = str1.substring(farthestSimilar);
        differentSubstrings[1] = str2.substring(farthestSimilar);

        return differentSubstrings;
    }
}