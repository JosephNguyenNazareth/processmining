package selfdevelop.utils;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import selfdevelop.casestudy.CaseStudy;
import selfdevelop.casestudy.Shopping;
import selfdevelop.extend.TermDetect;
import selfdevelop.simulate.Simulation;
import selfdevelop.transformation.Ontology;
import selfdevelop.transformation.ecommerce.OntoEcommerce;
import simplenlg.features.Feature;
import simplenlg.features.NumberAgreement;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.Lexicon;
import simplenlg.phrasespec.NPPhraseSpec;
import simplenlg.realiser.english.Realiser;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class SoftwareHeritageToXES extends Simulation {
    private void addContent (StringBuilder fileContent, MongoCollection<Document> events, CaseStudy realCaseStudy) {
        TermDetect termDetector = new TermDetect();
        StringBuilder content = new StringBuilder("\t<trace>\n");
        String currentProject = Objects.requireNonNull(events.find().first()).get("data_source").toString();

        for (Document event : events.find()) {
            if (!currentProject.equals(event.get("data_source").toString()))
                content.append("\t</trace>\n").append("\t<trace>\n");
            currentProject = event.get("data_source").toString();
            String rawEventName = event.get("event_name").toString();
            String filteredEventName = realCaseStudy.checkRelevant(rawEventName, termDetector);

            if (filteredEventName.equals("other"))
                continue;

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"event_id\" value=\"(3)\"/>\n" +
                    "\t\t\t<string key=\"data_source\" value=\"(4)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", event.get("user_name").toString()).replace("(1)", event.get("timestamp").toString()).replace("(2)", filteredEventName);
            eventInfo = eventInfo.replace("(3)", event.get("id").toString()).replace("(4)", event.get("data_source").toString());
            content.append(eventInfo);
        }
        content.append("\t</trace>\n");
        fileContent.append(content);
    }

    public void transform() {
        MongoCollection<Document> events = connectMongoDB("transformation","events");
        String processName = "EcommerceDev";
        String source = "software_heritage";

        CaseStudy ecommerce = new Shopping(processName);
        ecommerce.createConcept();

        StringBuilder fileContent = new StringBuilder(headerXES(processName, source));

        addContent(fileContent, events, ecommerce);

        fileContent.append(footerXES());
        saveFile(fileContent.toString(), processName, source,"./example-logs/" + processName);
    }

    private Map<String, String> addContentOnto (String processName, String source, Map<String, StringBuilder> objectContent, MongoCollection<Document> events, MongoCollection<Document> objects, OntoEcommerce onto) {
        TermDetect termDetector = new TermDetect();
        String currentProject = Objects.requireNonNull(events.find().first()).get("data_source").toString();
        Map<String, List<String>> objectUpdateList = new HashMap<>();
        Map<String, String> objectLatestProject = new HashMap<>();

        System.out.println("Start reading event execution traces...");
        Map<String, String> mappingRawFilteredName = new HashMap<>();
        for (Document event : events.find()) {
            Document object = objects.find(eq("id", event.get("id").toString())).first();
            if (object == null)
                continue;

            String rawEventName = event.get("event_name").toString();
            String filteredEventName = onto.detectTask(rawEventName, termDetector);

            if (filteredEventName.equals("other"))
                continue;

            List<Document> objectList = (List<Document>) object.get("diff");

            for (Document objectDoc : objectList) {
                String rawObjectName = objectDoc.get("path").toString();
                String filteredObjectName = onto.detectTask(rawObjectName, termDetector);

                if (!mappingRawFilteredName.containsKey(rawObjectName))
                    mappingRawFilteredName.put(rawObjectName, filteredObjectName);

                if (!objectContent.containsKey(filteredObjectName)) {
                    StringBuilder objectTraces = new StringBuilder(headerXES(processName, source + "_" + filteredObjectName));
                    objectTraces.append("\t<trace>\n");
                    objectContent.put(filteredObjectName, objectTraces);
                }
//                if (!currentProject.equals(event.get("data_source").toString()))
//                    objectContent.get(filteredObjectName).append("\t</trace>\n").append("\t<trace>\n");
//                currentProject = event.get("data_source").toString();

                if (!objectLatestProject.containsKey(filteredObjectName)) {
                    objectLatestProject.put(filteredObjectName, event.get("data_source").toString());
                } else if (!objectLatestProject.get(filteredObjectName).equals(event.get("data_source").toString())) {
                    objectContent.get(filteredObjectName).append("\t</trace>\n").append("\t<trace>\n");
                    objectLatestProject.put(filteredObjectName, event.get("data_source").toString());
                }

                String eventInfo = "\t\t<event>\n" +
                        "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                        "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                        "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                        "\t\t\t<string key=\"event_id\" value=\"(3)\"/>\n" +
                        "\t\t\t<string key=\"data_source\" value=\"(4)\"/>\n" +
                        "\t\t</event>\n";
                eventInfo = eventInfo.replace("(0)", event.get("user_name").toString()).replace("(1)", event.get("timestamp").toString()).replace("(2)", filteredEventName);
                eventInfo = eventInfo.replace("(3)", event.get("id").toString()).replace("(4)", event.get("data_source").toString());

                if (!objectUpdateList.containsKey(filteredObjectName)) {
                    List<String> listUpdate = new ArrayList<>();
                    listUpdate.add(event.get("id").toString());
                    objectUpdateList.put(filteredObjectName, listUpdate);
                    objectContent.get(filteredObjectName).append(eventInfo);
                } else if (!objectUpdateList.get(filteredObjectName).contains(event.get("id").toString())) {
                    objectUpdateList.get(filteredObjectName).add(event.get("id").toString());
                    objectContent.get(filteredObjectName).append(eventInfo);
                }
            }
        }

        for (String key : objectContent.keySet()) {
            objectContent.get(key).append("\t</trace>\n");
            objectContent.get(key).append(footerXES());
        }
        System.out.println("Complete reading event execution traces...");

        return mappingRawFilteredName;
    }

    private void addContentOnto (StringBuilder fileContent, MongoCollection<Document> events, OntoEcommerce onto) {
        TermDetect termDetector = new TermDetect();
        StringBuilder content = new StringBuilder("\t<trace>\n");
        String currentProject = Objects.requireNonNull(events.find().first()).get("data_source").toString();

        System.out.println("Start reading event execution traces...");
        for (Document event : events.find()) {
            if (!currentProject.equals(event.get("data_source").toString()))
                content.append("\t</trace>\n").append("\t<trace>\n");
            currentProject = event.get("data_source").toString();
            String rawEventName = event.get("event_name").toString();
            String filteredEventName = onto.detectTask(rawEventName, termDetector);

            if (filteredEventName.equals("other"))
                continue;

            String eventInfo = "\t\t<event>\n" +
                    "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                    "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                    "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                    "\t\t\t<string key=\"event_id\" value=\"(3)\"/>\n" +
                    "\t\t\t<string key=\"data_source\" value=\"(4)\"/>\n" +
                    "\t\t</event>\n";
            eventInfo = eventInfo.replace("(0)", event.get("user_name").toString()).replace("(1)", event.get("timestamp").toString()).replace("(2)", filteredEventName);
            eventInfo = eventInfo.replace("(3)", event.get("id").toString()).replace("(4)", event.get("data_source").toString());
            content.append(eventInfo);
        }
        content.append("\t</trace>\n");
        fileContent.append(content);

        System.out.println("Complete reading event execution traces...");
    }

    private void addContentObject(String processName, String source, Map<String, StringBuilder> objectContent, MongoCollection<Document> events, MongoCollection<Document> objects) {
        TermDetect termDetector = new TermDetect();
        String currentProject = Objects.requireNonNull(events.find().first()).get("data_source").toString();
        Map<String, List<String>> objectUpdateList = new HashMap<>();
        Map<String, String> objectLatestProject = new HashMap<>();

        System.out.println("Start reading event execution traces...");
        for (Document event : events.find()) {
            Document object = objects.find(eq("id", event.get("id").toString())).first();
            if (object == null)
                continue;

            String rawEventName = event.get("event_name").toString().replace("\"","'")
                    .replace("&","and");
            List<Document> objectList = (List<Document>) object.get("diff");

            for (Document objectDoc : objectList) {
                String rawObjectName = objectDoc.get("path").toString();
                if (!objectContent.containsKey(rawObjectName)) {
                    StringBuilder objectTraces = new StringBuilder(headerXES(processName, source + "_" + rawObjectName));
                    objectTraces.append("\t<trace>\n");
                    objectContent.put(rawObjectName, objectTraces);
                }
//                if (!currentProject.equals(event.get("data_source").toString()))
//                    objectContent.get(filteredObjectName).append("\t</trace>\n").append("\t<trace>\n");
//                currentProject = event.get("data_source").toString();

                if (!objectLatestProject.containsKey(rawObjectName)) {
                    objectLatestProject.put(rawObjectName, event.get("data_source").toString());
                } else if (!objectLatestProject.get(rawObjectName).equals(event.get("data_source").toString())) {
                    objectContent.get(rawObjectName).append("\t</trace>\n").append("\t<trace>\n");
                    objectLatestProject.put(rawObjectName, event.get("data_source").toString());
                }

                String eventInfo = "\t\t<event>\n" +
                        "\t\t\t<string key=\"org:resource\" value=\"(0)\"/>\n" +
                        "\t\t\t<date key=\"time:timestamp\" value=\"(1)\"/>\n" +
                        "\t\t\t<string key=\"concept:name\" value=\"(2)\"/>\n" +
                        "\t\t\t<string key=\"event_id\" value=\"(3)\"/>\n" +
                        "\t\t\t<string key=\"data_source\" value=\"(4)\"/>\n" +
                        "\t\t</event>\n";
                eventInfo = eventInfo.replace("(0)", event.get("user_name").toString()).replace("(1)", event.get("timestamp").toString()).replace("(2)", rawEventName);
                eventInfo = eventInfo.replace("(3)", event.get("id").toString()).replace("(4)", event.get("data_source").toString());

                if (!objectUpdateList.containsKey(rawObjectName)) {
                    List<String> listUpdate = new ArrayList<>();
                    listUpdate.add(event.get("id").toString());
                    objectUpdateList.put(rawObjectName, listUpdate);
                    objectContent.get(rawObjectName).append(eventInfo);
                } else if (!objectUpdateList.get(rawObjectName).contains(event.get("id").toString())) {
                    objectUpdateList.get(rawObjectName).add(event.get("id").toString());
                    objectContent.get(rawObjectName).append(eventInfo);
                }
            }
        }

        for (String key : objectContent.keySet()) {
            objectContent.get(key).append("\t</trace>\n");
            objectContent.get(key).append(footerXES());
        }
        System.out.println("Complete reading event execution traces...");
    }

    public void ontologyObjectTransform() {
        MongoCollection<Document> events = connectMongoDB("transformation","test_events3");
        MongoCollection<Document> objects = connectMongoDB("transformation","test_objects3");
        String processName = "EcommerceOntoDevObj";
        String source = "software_heritage";

        String dir = "/home/nguyenminhkhoi/Documents/doctorat/etude/ontology/software_implementation_ontology/software_engineering.owx";
        String iri = "http://www.semanticweb.org/nguyenminhkhoi/ontologies/2023/4/se";
        OntoEcommerce parser = new OntoEcommerce(dir, iri);

        Map<String, StringBuilder> objectContent = new HashMap<>();
        Map<String, String> mappingRawFiltered = addContentOnto(processName, source, objectContent, events, objects, parser);

        for (String objectName : objectContent.keySet())
            saveFile(objectContent.get(objectName).toString(), processName, source + "_" + objectName,"../processmining/example-logs/" + processName);

        try {
            StringBuilder mapping = new StringBuilder();
            for (String key: mappingRawFiltered.keySet()) {
                mapping.append(key).append("\t").append(mappingRawFiltered.get(key)).append("\n");
            }
            FileWriter myWriter = new FileWriter( "../processmining/example-logs/" + processName + "/" + processName + "_" + source + "_mapping.txt");
            myWriter.write(mapping.toString());
            myWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void ontologyTransform() {
        MongoCollection<Document> events = connectMongoDB("transformation","test_events");;
        String processName = "EcommerceOntoDev";
        String source = "software_heritage";

        String dir = "/home/nguyenminhkhoi/Documents/doctorat/etude/ontology/software_implementation_ontology/software_engineering.owx";
        String iri = "http://www.semanticweb.org/nguyenminhkhoi/ontologies/2023/4/se";
        OntoEcommerce parser = new OntoEcommerce(dir, iri);

        StringBuilder fileContent = new StringBuilder(headerXES(processName, source));
        addContentOnto(fileContent, events, parser);
        fileContent.append(footerXES());

        saveFile(fileContent.toString(), processName, source,"../processmining/example-logs/" + processName);
    }

    public void objectTransform() {
        MongoCollection<Document> events = connectMongoDB("transformation","test_events3");
        MongoCollection<Document> objects = connectMongoDB("transformation","test_objects3");
        String processName = "EcommerceDevObj";
        String source = "software_heritage";

        Map<String, StringBuilder> objectContent = new HashMap<>();
        addContentObject(processName, source, objectContent, events, objects);

        for (String objectName : objectContent.keySet())
            saveFile(objectContent.get(objectName).toString(), processName, source + "_" + objectName,"../processmining/example-logs/" + processName);
    }

    public static void main (String[] args) {
        SoftwareHeritageToXES handler = new SoftwareHeritageToXES();
//        handler.ontologyObjectTransform();
        handler.objectTransform();
//        TermDetect termDetector = new TermDetect();
//        String dir = "/home/nguyenminhkhoi/Documents/doctorat/etude/ontology/software_implementation_ontology/software_engineering.owx";
//        String iri = "http://www.semanticweb.org/nguyenminhkhoi/ontologies/2023/4/se";
//        OntoEcommerce parser = new OntoEcommerce(dir, iri);
//        String rawEventName = "set up credit card payment by taking into account the external source code";
//        String filteredEventName = parser.detectTask(rawEventName, termDetector);
//        System.out.println(filteredEventName);
    }
}
