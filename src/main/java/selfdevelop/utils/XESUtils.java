package selfdevelop.utils;

import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XESUtils {
    public static XLog readXESFile(String path) {
        File file = new File(path);

        XesXmlParser parser = new XesXmlParser();
        List<XLog> result = new ArrayList<XLog>();
        try {
            result = parser.parse(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.get(0);
    }
}
