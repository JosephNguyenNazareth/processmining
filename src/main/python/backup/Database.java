package com.simulation;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

public class Database {
    public static void saveMongoDb(String dbName, String collection, Document info) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        collect.insertOne(info);
        client.close();
    }

    public static Document loadMongoDb(String dbName, String collection, String fieldName, String value) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        return collect.find(eq(fieldName, value)).first();
    }

    public static Document loadMongoDb(String dbName, String collection, String fieldName, Long value) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        return collect.find(eq(fieldName, value)).first();
    }

    public static void clearCollectionMongoDb(String dbName, String collection) {
        MongoClient client = MongoClients.create("mongodb://localhost:27017");
        MongoDatabase database = client.getDatabase(dbName);

        MongoCollection<Document> collect = database.getCollection(collection);
        collect.drop();

        client.close();
    }
}
