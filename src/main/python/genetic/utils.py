import json
from pymongo import MongoClient

def read_json(path):
    f = open(path)
    data = json.load(f)
    f.close()

    return data

def read_file(path):
    f = open(path, "r")

    return f

def write_file(path, content):
    # content is a list of lines
    f = open(path, "w")
    f.writelines(content)
    f.close()

def append_file(path, content):
    # content is a list of lines
    f = open(path, "a")
    f.writelines(content)
    f.close()

def write_db(db_name, collection_name, data):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    if type(data) == list:
        collection.insert_many(data)
    elif type(data) == dict:
        collection.insert_one(data)

    client.close()

def read_db(db_name, collection_name, query):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    return collection.find(query), client

def read_db_one(db_name, collection_name, query):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    return collection.find_one(query), client

def drop_collection(db_name, collection_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    collection.drop()

    client.close()
