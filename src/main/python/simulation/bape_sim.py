import json
import random
from pymongo import MongoClient
import uuid


def write_db(db_name, collection_name, data):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    if type(data) == list:
        collection.insert_many(data)
    elif type(data) == dict:
        collection.insert_one(data)

    client.close()


def read_db(db_name, collection_name, query):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    return collection.find(query), client


def drop_collection(db_name, collection_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    collection.drop()

    client.close()


def read_tasks(json_file):
    f = open(json_file)
    data = json.load(f)

    return data


def first_step(project_id, real_project_id, process_name, time_now):
    info = {}
    info["type"] = "process"
    info["name"] = "Implement Function"
    info["id"] = project_id
    info["description"] = "AP" + str(real_project_id)
    info["actor"] = "PM1"
    info["status"] = "start"
    info["timestamp"] = time_now.isoformat(sep='T', timespec='milliseconds')
    info["processId"] = project_id
    info["process_title"] = process_name

    write_db("bape_gen", "process", info)

    return project_id


def third_step(project_id, process_name, actor, time_now):
    actor_activities = read_tasks("task.json")

    act_list = []
    for actor_def in actor_activities:
        if actor_def["actor"] == actor:
            act_list = actor_def["activities"]

    ready_to_write = []

    for activity in act_list:
        activity_id = uuid.uuid4().hex
        info_activity = {}
        info_activity["type"] = "activity"
        info_activity["name"] = activity["name"]
        info_activity["id"] = activity_id
        info_activity["actor"] = actor + "1"
        info_activity["status"] = "start"
        info_activity["timestamp"] = time_now.isoformat(
            sep='T', timespec='milliseconds')
        info_activity["processId"] = project_id
        info_activity["process_title"] = process_name

        ready_to_write.append(info_activity)

        for task_def in activity["tasks"]:
            task_id = uuid.uuid4().hex

            info_task = {}
            info_task["type"] = "task"
            info_task["name"] = task_def["name"]
            info_task["id"] = task_id
            info_task["actor"] = actor + "1"
            info_task["status"] = "start"
            info_task["timestamp"] = time_now.isoformat(
                sep='T', timespec='milliseconds')
            info_task["activityId"] = activity_id
            info_task["processId"] = project_id
            info_task["process_title"] = process_name

            ready_to_write.append(info_task)
    write_db("bape_gen", "process", ready_to_write)


def find_task_artifacts(actor, task, usage):
    actor_activities = read_tasks("task.json")

    possible_artifacts = []

    for actor_def in actor_activities:
        if actor_def["actor"] == actor:
            for act_def in actor_def["activities"]:
                for task_def in act_def["tasks"]:
                    if task_def["name"] == task:
                        for artifact_def in task_def["artifacts"]:
                            if artifact_def["usage"] == usage:
                                possible_artifacts.append(artifact_def)
                        return possible_artifacts

    return possible_artifacts


def fifth_step(bape_project_id, process_name, actor, time_now):
    query = {
        "actor": actor + "1",
        "type": "task",
        "status": "start",
        "processId": bape_project_id
    }
    original_info_task, client = read_db("bape_gen", "process", query)

    ready_to_write = []

    for possible_task in original_info_task:
        info_task = {}
        info_task["type"] = "task"
        info_task["name"] = possible_task["name"]
        info_task["id"] = possible_task["id"]
        info_task["actor"] = possible_task["actor"]
        info_task["status"] = "in progress"
        info_task["timestamp"] = time_now.isoformat(sep='T',
                                                    timespec='milliseconds')
        info_task["activityId"] = possible_task["activityId"]
        info_task["processId"] = possible_task["processId"]
        info_task["process_title"] = process_name

        possible_arifacts = find_task_artifacts(actor, info_task["name"],
                                                "ToStart")
        info_task["artifacts"] = [random.choice(possible_arifacts)]

        ready_to_write.append(info_task)

    client.close()
    write_db("bape_gen", "process", ready_to_write)


def seventh_step(bape_project_id, process_name, actor, time_now):
    query = {
        "actor": actor + "1",
        "type": "task",
        "status": "in progress",
        "processId": bape_project_id
    }
    original_info_task, client = read_db("bape_gen", "process", query)

    ready_to_write = []

    for possible_task in original_info_task:
        info_task = {}
        info_task["type"] = "task"
        info_task["name"] = possible_task["name"]
        info_task["id"] = possible_task["id"]
        info_task["actor"] = possible_task["actor"]
        info_task["status"] = "in progress"
        info_task["timestamp"] = time_now.isoformat(sep='T',
                                                    timespec='milliseconds')
        info_task["activityId"] = possible_task["activityId"]
        info_task["processId"] = possible_task["processId"]

        possible_arifacts = find_task_artifacts(actor, info_task["name"],
                                                "ToFinish")
        info_task["artifacts"] = [random.choice(possible_arifacts)]
        info_task["process_title"] = process_name

        ready_to_write.append(info_task)

    client.close()
    write_db("bape_gen", "process", ready_to_write)


def clear_collection(process_name, db_name, collection_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client[db_name]
    collection = db[collection_name]

    query = {"process_title": process_name}
    collection.delete_many(query)

    client.close()