# we have to run the simulation in order, oh yes, we have to run in order with some anomalies
# not in fully random stuffs
import pytz
import bape_sim
import jira_sim
from gitlab_sim import *

# real_project_id = [768791, 695500, 886010, 514723, 840507, 452873, 475827, 126726, 446011, 526598, 536902, 787636]
local_timezone = pytz.timezone('Europe/Paris')
process_name = "AppDev"


def bape_simulation(bape_project_id, proj_id, actor, time_now):
    bape_sim.first_step(bape_project_id, proj_id, process_name, time_now)

    time_now += timedelta(minutes=random.randrange(10, 400))
    bape_sim.third_step(bape_project_id, process_name, actor, time_now)

    time_now += timedelta(minutes=random.randrange(10, 400))
    bape_sim.fifth_step(bape_project_id, process_name, actor, time_now)

    time_now += timedelta(minutes=random.randrange(10, 400))
    bape_sim.seventh_step(bape_project_id, process_name, actor, time_now)

    return time_now


def bape_simulation2(bape_project_id, actor, time_now):
    bape_sim.third_step(bape_project_id, process_name, actor, time_now)

    time_now += timedelta(minutes=random.randrange(10, 400))
    bape_sim.fifth_step(bape_project_id, process_name, actor, time_now)

    time_now += timedelta(minutes=random.randrange(10, 400))
    bape_sim.seventh_step(bape_project_id, process_name, actor, time_now)

    return time_now


def run_analyst(proj_id, time_now):
    return jira_sim.load_issue_mongo(proj_id, process_name, time_now)


def run_architecture_designer(bape_project_id, proj_id, time_now):
    actor = "Architecture Designer"
    return bape_simulation(bape_project_id, proj_id, actor, time_now)


def run_developer(proj_id, time_now):
    return load_commit_mongo(proj_id, process_name, time_now)


def run_tester(bape_project_id, time_now):
    actor = "Tester"
    return bape_simulation2(bape_project_id, actor, time_now)


def create_real_project_id(nb_project):
    real_project_id = []
    count = 0

    while count < nb_project:
        tmp = random.randint(100000, 999999)
        if tmp not in real_project_id:
            real_project_id.append(tmp)
            count += 1

    return real_project_id


def main():
    nb_instance = 20
    current_project_list = create_real_project_id(nb_instance)

    # very first cleaning
    clear_collections_gitlab(process_name)
    jira_sim.clear_collections_jira(process_name)
    bape_sim.clear_collection(process_name, 'bape_gen', 'process')

    # one instance
    for _ in range(nb_instance):
        project_id = random.choice(current_project_list)
        current_project_list.remove(project_id)

        time_now = datetime.now(local_timezone)
        time_now = run_analyst(project_id, time_now)

        bape_project_id = uuid.uuid4().hex

        time_now += timedelta(minutes=random.randrange(100, 600))
        time_now = run_architecture_designer(bape_project_id, project_id,
                                             time_now)

        time_now += timedelta(minutes=random.randrange(100, 600))
        time_now = run_developer(project_id, time_now)

        time_now += timedelta(minutes=random.randrange(100, 600))
        time_now = run_tester(bape_project_id, time_now)


main()