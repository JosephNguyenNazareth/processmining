from pymongo import MongoClient
import random
from datetime import datetime, timedelta
import uuid
from dateutil.parser import *


def check_transfer_design(state, artifact_name, current_issue):
    return state == "Transfer" and artifact_name == "ArchitectureModificationRequest" and current_issue not in [
        "Make first version", "Review first version"
    ]


def check_transfer_sourcecode(state, artifact_name):
    return state == "Transfer" and artifact_name == "SourceCode"


def update_artifact_state(artifact_states, current_issue, transfer_code,
                          for_title):
    verb, name = for_title[0], for_title[1]
    state = ""
    while True:
        artifact_name = random.choice(name)
        if artifact_name not in artifact_states:
            state = verb[0]
            if transfer_code == 0:
                artifact_states[artifact_name] = [state]
                break
            elif not check_transfer_design(state, artifact_name,
                                           current_issue):
                artifact_states[artifact_name] = [state]
                break
        elif artifact_states[artifact_name][len(artifact_states[artifact_name])
                                            - 1] == verb[len(verb) - 1]:
            state = verb[0]
            if transfer_code == 0:
                artifact_states[artifact_name].append(state)
                break
            elif not check_transfer_design(state, artifact_name,
                                           current_issue):
                artifact_states[artifact_name].append(state)
                break
        else:
            state = random.choice(verb[1:])
            if transfer_code == 0:
                if check_transfer_sourcecode(state, artifact_name):
                    artifact_states[artifact_name].append(state)
                    transfer_code = -1
                    break
            elif not check_transfer_design(state, artifact_name,
                                           current_issue):
                artifact_states[artifact_name].append(state)
                break

    return artifact_name, state, artifact_states, transfer_code


def combine_commit_title(artifact_states, issue, transfer_code, for_title):
    chosen_name, chosen_verb, artifact_states, transfer_code = update_artifact_state(
        artifact_states, issue, transfer_code, for_title)
    return [chosen_verb, chosen_name], artifact_states, transfer_code


def gen_project_gitlab(process_name, real_id, project_id_gitlab):
    project_name = "AP"
    project_info = {}

    project_info["id"] = project_id_gitlab
    project_info["description"] = process_name
    project_info["name"] = "project {}{}".format(project_name, str(real_id))

    return project_info


def create_issue_gitlab(ref_id, project_id, time_now, committer, issue_list):
    issue = {}

    issue["iid"] = ref_id
    issue["project_id"] = project_id
    issue["title"] = issue_list[ref_id - 1]
    issue["author"] = choose_committer(committer)
    issue["created_at"] = time_now.isoformat(sep='T', timespec='milliseconds')
    issue["closed_at"] = "null"
    issue["closed_by"] = "null"

    return issue


def close_issue_gitlab(issue, time_now, committer):
    issue["closed_at"] = time_now.isoformat(sep='T', timespec='milliseconds')
    issue["closed_by"] = random.choice(committer)

    return issue

def choose_committer(committer):
    prob_anomalies = random.random()
    committer_now = ""
    if prob_anomalies > 0.95:
        committer_now = committer[-1]
    else:
        committer_now = random.choice(committer[:-1])

    return committer_now


def gen_commit_gitlab(time_now, proj_id, process_name, committer, issue_list, for_title):
    project_id = random.randrange(10000000, 100000000)

    projects = []
    issues_all = []
    commits = []
    diffs = []

    commit_range = random.randrange(6, 21)

    # create random number of commit belonging to each issue
    issues = []
    issue_range = []
    issue_range.append(random.randrange(1, commit_range - 2))
    issue_range.append(issue_range[0] +
                       random.randrange(1, commit_range - issue_range[0] - 1))
    issue_range.append(commit_range)

    # create a state machine for artifact
    artifact_states = {}

    # create a unique random project id for gitlab
    project_id = uuid.uuid4().hex
    projects.append(gen_project_gitlab(process_name, proj_id, project_id))

    # add first issue to the issue list
    issue_count = 1
    issues.append(
        create_issue_gitlab(issue_count, project_id, time_now, committer,
                            issue_list))

    transfer_code = random.randint(1, 100)

    for i in range(commit_range):
        # for issue create during the commit generation
        if i == issue_range[issue_count - 2]:
            issues.append(
                create_issue_gitlab(issue_count, project_id, time_now,
                                    committer, issue_list))

        # details info for each commit
        petite_commit = {}
        petite_commit["id"] = uuid.uuid4().hex
        petite_commit["created_at"] = time_now.isoformat(sep='T', timespec='milliseconds')

        title_token, artifact_states, transfer_code = combine_commit_title(
            artifact_states, issue_list[issue_count - 1], transfer_code,
            for_title)

        if transfer_code not in [-1, 0, 1]:
            transfer_code = random.randint(1, transfer_code)
        elif transfer_code == 1:
            transfer_code = 0

        petite_commit["title"] = " ".join(title_token)

        petite_commit["committer_name"] = choose_committer(committer)
        petite_commit["description"] = "for issue #{}".format(str(issue_count))
        petite_commit["project_id"] = project_id

        commits.append(petite_commit)

        # create differences info list for each commit
        diffs.append(gen_diff_gitlab(petite_commit, title_token))

        # for issue close
        if i == issue_range[issue_count - 1] - 1:
            issues[issue_count - 1] = close_issue_gitlab(
                issues[issue_count - 1], time_now, committer)
            issue_count += 1

        # adding random time change
        rand_time_change = random.randrange(10, 600)
        time_change = timedelta(minutes=rand_time_change)
        time_now += time_change
    issues_all += issues

    return projects, issues_all, commits, diffs, time_now


def load_commit_mongo(proj_id, process_name, time_now):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client['gitlab_gen']

    collection_projects = db['projects']
    collection_issues = db['issues']
    collection_commits = db['commits']
    collection_diffs = db['diffs']

    committer = ["JosephNguyenNazareth", "SunnyCherry", "Tester1"]
    verb = ["Create", "Modify", "Move", "Rename", "Transfer", "Delete"]
    name = [
        "ArchitectureDesign", "Report", "ArchitectureModificationRequest",
        "SourceCode", "function", "screenshot", "note"
    ]
    issue_list = [
        "Make first version", "Review first version", "Make final version"
    ]

    projects_result, issue_result, commit_result, diff_result, time_now = gen_commit_gitlab(
        time_now, proj_id, process_name, committer, issue_list, [verb, name])

    collection_projects.insert_many(projects_result)
    collection_issues.insert_many(issue_result)
    collection_commits.insert_many(commit_result)
    collection_diffs.insert_many(diff_result)

    client.close()

    return time_now


def clear_collections_gitlab(process_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client['gitlab_gen']

    collection_projects = db['projects']
    collection_issues = db['issues']
    collection_commits = db['commits']
    collection_diffs = db['diffs']

    # search the name of the rerun project
    # follow the project id to
    # 1. find the corresponding commits of the project
    # then delete each commit diff and commit itself
    # 2. delete the the issues having the project id
    project_query = {"description": process_name}
    for instance in collection_projects.find(project_query):
        project_id = instance["id"]
        project_id_query = {"project_id": project_id}

        for commit in collection_commits.find(project_id_query):
            commit_id = commit["id"]
            commit_id_query = {"id": commit_id}
            collection_diffs.delete_many(commit_id_query)
        collection_commits.delete_many(project_id_query)
        collection_issues.delete_many(project_id_query)
    
    collection_projects.delete_many(project_query)

    # collection_projects.drop()
    # collection_issues.drop()
    # collection_commits.drop()
    # collection_diffs.drop()

    client.close()


def gen_diff_gitlab(commit, token):
    diff = {}
    diff["id"] = commit["id"]
    diff["diff"] = []

    obj_info = {}
    obj_info["old_path"] = token[1]
    obj_info["new_path"] = token[1]
    obj_info["new_file"] = True if token[0] == "Create" else False
    obj_info["renamed_file"] = True if token[0] == "Rename" else False
    obj_info["deleted_file"] = True if token[0] == "Delete" else False

    diff["diff"].append(obj_info)

    return diff