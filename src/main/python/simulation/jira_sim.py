from pymongo import MongoClient
import random
from datetime import timedelta
from dateutil.parser import *

# this function is for mapping the name of the artifact with the corresponding issue
def combine_issue_title(for_title, issue_name):
    verb, obj, name = for_title[0], for_title[1], for_title[2]
    chosen_verb = random.choice(verb)
    chosen_obj = random.choice(obj)
    chosen_name = random.choice(name)
    # print(issue_name, chosen_name)
    while True:
        # in reality, the demands just takes part in the first issue only
        if issue_name != "Collect requirements" and chosen_name == "Demands":
            chosen_name = random.choice(name)
            # print(chosen_name)
        else:
            break

    # print("Final " + chosen_name)
    return [chosen_verb, chosen_obj, chosen_name]

# this functions aims to configure
# the number of issue created
# how these issues progressed
def init_project():
    issue_range = random.randrange(10, 21)
    issue_create_range = random.randrange(6, issue_range - 3)

    issue_range_x = []
    issue_range_x.append(random.randrange(1, issue_create_range - 2))
    issue_range_x.append(issue_range_x[0] + random.randrange(1, issue_create_range - issue_range_x[0] - 1))
    issue_range_x.append(issue_create_range)

    return issue_range, issue_range_x, issue_create_range

# this functions is to generate the event log for jira
def gen_issue(process_name, time_now, real_id, issue_list, assignee, status, for_title):
    project_name = "AP"
    issues = []
    
    creating = True
    issue_range, issue_range_x, issue_create_range = init_project()
    counter_id = 0
    
    issues_instance = []
    project_name = "AP"

    # add first issue to the issue list
    issue_count = 0

    for i in range(issue_range):
        issue = {}
        if creating:
            issue["key"] = "{}{}-{}".format(project_name,str(real_id),str(counter_id))
            # title_token = combine_issue_title(for_title, issue_list[issue_count])
            issue["summary"] = issue_list[issue_count]
            issue["issue_type"] = "Task"
            issue["assignee"] = random.choice(assignee)
            issue["created"] = time_now.isoformat(sep='T', timespec='milliseconds')
            issue["status"] = "To do"
            issue["status_changed"] = time_now.isoformat(sep='T', timespec='milliseconds')
            issue["project"] = process_name
        else:
            for check_issue in issues_instance:
                if check_issue["key"] == "{}{}-{}".format(project_name,str(real_id),str(counter_id)):
                    issue["key"] = "{}{}-{}".format(project_name,str(real_id),str(counter_id))
                    title_token = combine_issue_title(for_title, check_issue["summary"])
                    issue["summary"] = check_issue["summary"]
                    issue["issue_type"] = "Task"
                    issue["assignee"] = random.choice(assignee)
                    issue["created"] = check_issue["created"]
                    issue["status"] = random.choice(status)
                    issue["status_changed"] = time_now.isoformat(sep='T', timespec='milliseconds')
                    issue["project"] = check_issue["project"]
                    if title_token[1] == "file":
                        issue["attachment"] = [title_token[2]]
                    break

        issues_instance.append(issue)
        issues.append(issue)

        # for issue close
        if  i == issue_range_x[issue_count] - 1:
            if issue_count < 2:
                issue_count += 1

        rand_time_change = random.randrange(10, 600)
        time_change = timedelta(minutes=rand_time_change)
        time_now += time_change

        if counter_id != issue_create_range - 1 and creating:
            counter_id += 1
        else:
            counter_id = random.randrange(0, issue_create_range)
            creating = False

    return issues, time_now

def load_issue_mongo(proj_id, process_name, time_now):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client['jira_gen']

    collection_issues = db['issues']
    # collection_issues.drop()

    # some necessary info
    assignee = ["Minh Khoi NGUYEN", "Sunny Night"]
    verb = ["Create", "Delete", "Modify", "Move", "Rename"]
    obj = ["file"]
    name = ["Demands", "Requirements", "screenshot", "note"]
    status = ["In progress", "Done"]
    issue_list = ["Collect requirements", "Edit requirements", "Export requirements"]

    issue_result, time_now = gen_issue(process_name, time_now, proj_id, issue_list, assignee, status, [verb, obj, name])
    collection_issues.insert_many(issue_result)

    client.close()

    return time_now


def clear_collections_jira(process_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    # predefined variables
    db = client['jira_gen']
    
    collection_issues = db['issues']
    project_query = {"project": process_name}

    collection_issues.delete_many(project_query)

    client.close()