from pymongo import MongoClient
import pymongo
import os
import json

try:
    client = MongoClient('localhost', 27017)
    print("Connected")
except:  
    print("Error")

db = client['gitlab']
project_id = "33950365"

def get_commit_gitlab():
    result = os.popen("curl https://gitlab.com/api/v4/projects/" + project_id + "/repository/commits").read()
    commit_result = json.loads(result)
    for commit in commit_result:
        commit["project_id"] = int(project_id)

    return commit_result

def load_commit_mongo():
    collection = db['commits']
    # collection.create_index([('id', pymongo.ASCENDING)], unique=True)
    json_result = get_commit_gitlab()
    
    collection.insert_many(json_result)

def load_commit_diff_mongo():
    collection = db['diff']
    # collection.create_index([('id', pymongo.ASCENDING)], unique=True)
    json_result = get_commit_gitlab()
    
    for commit in json_result:
        get_command = "curl https://gitlab.com/api/v4/projects/{id}/repository/commits/{sha}/diff".format(id = project_id, sha = commit["id"])
        result = os.popen(get_command).read()
        commit_diff = json.loads(result)
        commit_diff_w_id = {"id": commit["id"], "diff": commit_diff}
         
        collection.insert_one(commit_diff_w_id)

def load_issue_mongo():
    collection = db['issue']
    # collection.create_index([('id', pymongo.ASCENDING)], unique=True)
    get_command = "curl https://gitlab.com/api/v4/projects/{id}/issues".format(id = project_id)
    result = os.popen(get_command).read()
    issues = json.loads(result)

    collection.insert_many(issues)

def load_user_mongo():
    collection = db['user']
    # collection.create_index([('id', pymongo.ASCENDING)], unique=True)
    get_command = "curl https://gitlab.com/api/v4/projects/{id}/users".format(id = project_id)
    result = os.popen(get_command).read()
    users = json.loads(result)

    collection.insert_many(users)

load_user_mongo()