# import the installed Jira library
from cmath import sin
from jira import JIRA
  
# Specify a server key. It should be your
# domain name link. yourdomainname.atlassian.net
jiraOptions = {'server': "https://generatecode.atlassian.net"}
  
# Get a JIRA client instance, pass,
# Authentication parameters
# and the Server name.
# emailID = your emailID
# token = token you receive after registration
jira = JIRA(options=jiraOptions, basic_auth=(
    "ngmkhoisg@gmail.com", "6UiAehgwmyHEtBzlfau80C06"))
  
# Search all issues mentioned against a project name.
# for singleIssue in jira.search_issues(jql_str='project="Generate Code with Genetic Algorithm"'):
    # singleIssue = jira.issue(singleIssue.key)
singleIssue = jira.issue("GCWGA-5")
print('{} - {} - {} - {} - {} - {} - {} - {}'.format(singleIssue.key,
                        singleIssue.fields.summary,
                        singleIssue.fields.issuetype,
                        singleIssue.fields.reporter,
                        singleIssue.fields.assignee,
                        singleIssue.fields.created,
                        singleIssue.fields.status,
                        singleIssue.fields.statuscategorychangedate))
for attachment in singleIssue.fields.attachment:
    print("Name: '{filename}', size: {size}".format(
        filename=attachment.filename, size=attachment.size))