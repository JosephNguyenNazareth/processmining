import function_sh
import function_repo
from pymongo import MongoClient
import sys

def save_data(project_name, project_result, commit_result, diff_result):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    db = client[project_name]

    collection_projects = db['projects']
    collection_commits = db['commits']
    collection_diffs = db['diffs']

    collection_projects.insert_many(project_result)
    collection_commits.insert_many(commit_result)
    collection_diffs.insert_many(diff_result)

    client.close()

def clear_data(project_name):
    try:
        client = MongoClient('localhost', 27017)
    except:
        print("Error")

    db = client[project_name]

    collection_projects = db['projects']
    collection_commits = db['commits']
    collection_diffs = db['diffs']

    collection_projects.drop()
    collection_commits.drop()
    collection_diffs.drop()

    client.close()

project_name = "software_heritage_test"
source = "./shopping_sh_small.txt"
# clear_data(project_name)
user_request = "JosephNguyenNazareth"
access_token = "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"

keyword = sys.argv[1]
nb_result = sys.argv[2]

collection = function_sh.load("{}_{}.txt".format(keyword, nb_result))
projects = function_repo.tag_project(collection, keyword)
commits = []

count = 1
for proj in projects:
    result_commit = function_repo.get_commits_github(proj["id"], user_request, access_token)
    print(count, proj["id"])
    count += 1
    if type(result_commit) == list:
        commits += result_commit

diffs = []
for commit in commits:
    result_diff = function_repo.get_diff_github(commit["project_id"], commit["id"], user_request, access_token)
    if type(result_diff) == dict:
        diffs.append(result_diff)

save_data(project_name, projects, commits, diffs)