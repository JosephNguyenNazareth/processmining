import requests
from datetime import datetime
import pytz
import json
from requests.auth import HTTPBasicAuth
from unidecode import unidecode

def tag_project(project_url_list, keyword):
    list_project = []
    for proj in project_url_list:
        if "github.com" not in proj:
            continue

        proj_info = {}
        proj_info["id"] = proj
        proj_info["source"] = "github"
        proj_info["keyword"] = keyword
        list_project.append(proj_info)

    print("We have {} project saved.".format(len(list_project)))

    return list_project

def get_commits_github(project_url, user, token):
    prefix_url = "api.github.com/repos"
    postfix_url = "/commits"

    api_url = project_url.replace("github.com", prefix_url) + postfix_url

    try:
        resp = requests.get(api_url, auth = HTTPBasicAuth(user, token))
        good_form = unidecode(resp.text)
        origins = json.loads(good_form)
    except (json.decoder.JSONDecodeError, json.JSONDecodeError):
        return

    if type(origins) != list:
        return []
        
    extract_commits = []
    local_tz = pytz.timezone("Europe/Paris")

    # print(api_url)

    for commit in origins:
        extract_info = {}
        extract_info["id"] = commit["sha"]

        tmp_time = commit["commit"]["committer"]["date"]
        tmp_time = datetime.strptime(tmp_time, "%Y-%m-%dT%H:%M:%SZ")
        tmp_time = tmp_time.astimezone(local_tz)
        extract_info["created_at"] = tmp_time.isoformat(sep='T', timespec='milliseconds')
        
        extract_info["title"] = commit["commit"]["message"].strip()
        extract_info["committer_name"] = commit["commit"]["committer"]["name"].strip()
        extract_info["project_id"] = project_url

        extract_commits.append(extract_info)

    return extract_commits


def get_diff_github(project_url, commit_sha, user, token):
    prefix_url = "api.github.com/repos"
    postfix_url = "/commits/"

    api_url = project_url.replace("github.com", prefix_url) + postfix_url + commit_sha
    
    try:
        resp = requests.get(api_url, auth = HTTPBasicAuth(user, token))
        good_form = unidecode(resp.text)
        origins = json.loads(good_form)
    except (json.decoder.JSONDecodeError, json.JSONDecodeError):
        return

    commit_info = {}
    commit_info["id"] = commit_sha
    commit_info["diff"] = []

    # print(api_url)

    for file_diff in origins["files"]:
        extract_info = {}
        extract_info["path"] = file_diff["filename"]
        extract_info["status"] = file_diff["status"]
        extract_info["changes"] = file_diff["changes"]

        commit_info["diff"].append(extract_info)

    return commit_info