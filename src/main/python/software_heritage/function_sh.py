import requests
import sys

def browse(keyword, nb_results):
    resp = requests.get("https://archive.softwareheritage.org/api/1/origin/search/{}?limit={}".format(keyword, nb_results))
    origins = resp.json()
    print(f"We found {len(origins)} entries.")

    with open("{}_{}.txt".format(keyword, nb_results), "w") as result_file:
        for result in origins:
            result_file.write(result["url"] + "\n")

    # return [x['url'] for x in origins]

def load(source):
    file1 = open(source, 'r')
    lines = file1.readlines()

    print(lines)
    
    count = 0
    urls = []
    for line in lines:
        count += 1
        urls.append(line.strip())

    print(f"We found {count} entries.")

    return urls

browse(sys.argv[1], int(sys.argv[2]))