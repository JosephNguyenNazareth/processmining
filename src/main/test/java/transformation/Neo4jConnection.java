package transformation;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.neo4j.driver.Driver;
import selfdevelop.transformation.Neo4jUtils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Neo4jConnection {
    @Test
    void createDriverAndConnectToServer() {
        Neo4jUtils.loadProperties();
        assertNotNull(Neo4jUtils.getNeo4jUri(), "neo4j uri defined");
        assertNotNull(Neo4jUtils.getNeo4jUsername(), "username defined");
        assertNotNull(Neo4jUtils.getNeo4jPassword(), "password defined");

        Driver driver = Neo4jUtils.initDriver();
        Assumptions.assumeTrue(driver != null);
        assertNotNull(driver, "driver instantiated");
        assertDoesNotThrow(driver::verifyConnectivity,"unable to verify connectivity");
    }
}
